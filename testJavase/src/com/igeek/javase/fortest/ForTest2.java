package com.igeek.javase.fortest;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/7/11 20:09
 * @description 金字塔
 * 2.在屏幕上打印出n行的金字塔图案，如，若n=5,则图案如下：
 *         *
 *        ***
 *       *****
 *      *******
 *     *********
 * 拓展：打印输出
 * 			A
 * 		   BBB
 * 		  CCCCC
 * 	     DDDDDDD
 * 	    EEEEEEEEE
 *
 */
public class ForTest2 {
    //打印金字塔图案
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入行数：");
        int col = scanner.nextInt();
        for (int i=1;i<=col;i++) {//i表示行数
            //打印空格
            for (int k=0;k<col-i;k++) {
                System.out.print(" ");
            }
            //打印星星
            for (int m=0;m<2*i-1;m++) {
                System.out.print("*");
            }
            System.out.println();
        }
        print();
    }

    public static void print() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入行数：");
        int col = scanner.nextInt();
        for (int i=1; i<=col;i++) {//i表示行数
            //打印空格
            for (int k=0; k<col-i;k++) {
                System.out.print(" ");
            }
            //打印字母
            for (int m=0; m<2*i-1;m++) {
                char c = (char)('A'+(i-1));
                System.out.print(c);
                }

            System.out.println();
        }
    }
}


