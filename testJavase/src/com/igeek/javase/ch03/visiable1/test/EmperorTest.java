package com.igeek.javase.ch03.visiable1.test;

/**
 * @author wangjin
 * 2023/7/13 13:05
 * @description TODO
 */
public class EmperorTest {
    public static void main(String[] args) {
        Emperor emperor1 = new Emperor("嬴政","前259年—前210年","统一六国","中国第一位皇帝");
        Emperor emperor2 = new Emperor("唐太宗","599年—649年","开创贞观之治","千古一帝");
        System.out.println(emperor1);
        System.out.println(emperor2);
    }
}
