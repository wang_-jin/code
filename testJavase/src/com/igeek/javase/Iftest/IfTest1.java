package com.igeek.javase.Iftest;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/7/11 16:11
 * @description if示例
 */
public class IfTest1 {
    /*
     * 1.需求：if示例,从键盘接收两个数字,再接收一个符号,然后进行运算
     * -------请输入第一个数字:8
     * -------请输入第二个数字:9
     * -------请输入运算符号:+
     * -------8+9=17
     */
    public static void main(String[] args) {
        //创建Scanner对象，获取两个数字
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入第一个数字");
        int i = sc.nextInt();
        System.out.println("请输入第二个数字");
        int j = sc.nextInt();
        //获取一个运算符号
        System.out.println("请输入一个运算符号");
        String s = sc.next();
        System.out.println("s = "+s);
        //进行运算
        if (s.equals("+")) {
            System.out.println("i + j = " +(i + j));
        } else if (s.equals("-")) {
            System.out.println("i - j = " + (i - j));
        } else if (s.equals("*")) {
            System.out.println("i * j = " + (i * j));
        } else if (s.equals("/")) {
            System.out.println("i / j = " + (i / j));
        } else{
            System.out.println("运算符号不存在~");
        }

    }

}
