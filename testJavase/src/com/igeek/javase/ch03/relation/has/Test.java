package com.igeek.javase.ch03.relation.has;

/**
 * @author wangjin
 * 2023/7/13 17:46
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Student stu1 = new Student("小王同学" , 2);
        Student stu2 = new Student("小李同学" , 3);
        Student stu3 = new Student("小刘同学" , 1);
        Student[] stus = {stu1 , stu3};

        //为班级添加学生
        Classes classes = new Classes(101 , "Java班" , stus);
        //为学生分配班级
        stu1.setClasses(classes);
        stu3.setClasses(classes);

        //显示班级信息
        classes.showClasses();
    }
}
