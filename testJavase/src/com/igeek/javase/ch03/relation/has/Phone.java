package com.igeek.javase.ch03.relation.has;

/**
 * @author wangjin
 * 2023/7/13 12:33
 * @description TODO
 */
public class Phone {
    private String label;
    private String color;
    private double price;

    public Phone() {
    }

    public Phone(String label, String color, double price) {
        this.label = label;
        this.color = color;
        this.price = price;
    }

    /**
     * 获取
     * @return label
     */
    public String getLabel() {
        return label;
    }

    /**
     * 设置
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * 获取
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * 设置
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * 获取
     * @return price
     */
    public double getPrice() {
        return price;
    }

    /**
     * 设置
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    public String toString() {
        return "Phone{label = " + label + ", color = " + color + ", price = " + price + "}";
    }
}
