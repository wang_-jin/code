package com.igeek.javase.ch05.jdbc.utils;

import java.sql.*;

/**
 * @author chenmin
 * 2023/7/19 15:33
 * @description TODO
 */
public class JDBCUtils {

    //获取连接对象的方法
    public static Connection getConn(){
        try {
            //1.加载驱动类
            Class.forName("com.mysql.cj.jdbc.Driver");
            //2.获取连接对象
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/testdql?useUnicode=true&characterEncoding=utf8&serverTimezone=Asia/Shanghai" ,
                    "root" , "root");
            return connection;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //关闭资源
    public static void close(Connection conn , Statement st , ResultSet rs){
        if(rs!=null){
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(st!=null){
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(conn!=null){
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
