package com.igeek.javase.ch03.extendss;

/**
 * @author wangjin
 * 2023/7/12 17:40
 * @description TODO
 */
public class A {
    public A(){
        super();
        this.show();
    }
    public void show(){
        System.out.println("A");
    }
}
class B extends A{
    public B(){
        super();
        this.show();
    }
    public void show(){
        System.out.println("B");
    }
}
class C extends B{
    public C(){
        super();
        this.show();
    }
    public void show(){
        System.out.println("C");
    }
}
