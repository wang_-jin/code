package com.igeek.javase.switchtest;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/7/11 19:03
 * @description 成绩的级别
 * 1.输入学员的考试成绩，输出这学员考试成绩的级别。
 * 100      A级
 * 90~99    A级
 * 80~89    B级
 * 70~79    C级
 * 60~69    D级
 * < 60     不合格
 */
public class SwitchTest1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("输入考生成绩：");
        int score = sc.nextInt();
        if (score>100||score<0) {
            System.out.println("输入有误");
            return;//直接退出
        }

        //成绩级别判定
        switch(score/10){
            case 10:
            case 9:
                System.out.println("A");
                break;
            case 8:
                System.out.println("B");
                break;
            case 7:
                System.out.println("C");
                break;
            case 6:
                System.out.println("D");
                break;
            case 5:
            case 4:
            case 3:
            case 2:
            case 1:
            case 0:
                System.out.println("不合格");
                break;
        }

    }
}
