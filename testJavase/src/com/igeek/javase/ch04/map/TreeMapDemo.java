package com.igeek.javase.ch04.map;

import java.util.Set;
import java.util.TreeMap;

/**
 * @author wangjin
 * 2023/7/17 18:37
 * @description TODO
 * TreeMap
 * 1.以键值对存储 key-value
 * 2.对key键进行排序
 * 3.不能存放null键，允许存放null值
 * 4.一旦键重复，值会发生覆盖
 * 5.线程不安全
 */
public class TreeMapDemo {

    public static void main(String[] args) {
        TreeMap<Integer , String> map = new TreeMap<>();
        map.put(10 , "abc");
        map.put(30 , "abd");
        map.put(14 , "abb");
        map.put(22 , "abc");
        // map.put(null , "abc");  不允许存放null键
        map.put(11, null);
        map.put(10 , "aba");
        Set<Integer> keys = map.keySet();
        for (Integer key : keys) {
            String value = map.get(key);
            System.out.println("key = "+key+" , value = "+value);
        }
    }
}

