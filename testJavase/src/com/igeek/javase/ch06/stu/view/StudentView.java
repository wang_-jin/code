package com.igeek.javase.ch06.stu.view;

import com.igeek.javase.ch06.stu.dao.AdminDao;
import com.igeek.javase.ch06.stu.dao.StudentDao;
import com.igeek.javase.ch06.stu.entity.Admin;
import com.igeek.javase.ch06.stu.entity.Student;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/7/20 21:57
 * @description TODO
 */
public class StudentView {
    public static void main(String[] args) throws SQLException, ParseException {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入管理员姓名");
        String name = sc.next();
        System.out.println("请输入管理员密码");
        String pw = sc.next();
        AdminDao dao = new AdminDao();
        StudentDao studentDao = new StudentDao();



        boolean flag = dao.login(name,pw);
        System.out.println(flag?"登录成功":"登录失败");

        while(flag){
            //显示初始化界面
            init();
            System.out.println("请输入操作符：");
            int i = sc.nextInt();

            switch (i){
                case 0:
                    System.out.println("欢迎使用，下次再来");
                    System.exit(0);
                    break;
                case 1:
                    System.out.println("1、 统计学生人数");
                    int count = studentDao.selectCount();
                    System.out.println("count = "+count);
                    break;
                case 2:
                    System.out.println("2、 查看学生名单");
                    List<Student> students = studentDao.selectStus("");
                    for (Student s : students) {
                        System.out.println(s);
                    }
                    break;
                case 3:
                    System.out.println("3、 按学号查询学生姓名");
                    System.out.println("请输入学号：");
                    String id = sc.next();
                    Student student = studentDao.selectOneById(id);
                    System.out.println(id+" 号的学生姓名为："+student.getName()+"，所在年级："+student.getGender());
                    break;
                case 4:
                    System.out.println("4、 按姓名查询学生信息");
                    System.out.println("请输入学生名称：");
                    String query = sc.next();
                    students = studentDao.selectStus(query);
                    for (Student s : students) {
                        System.out.println(s);
                    }
                    break;
                case 5:
                    System.out.println("5、 修改学生出生日期");
                    System.out.println("请输入学生学号：");
                    id = sc.next();
                    System.out.println("请输入学生生日：");
                    String birthdayStr = sc.next();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date birthday = sdf.parse(birthdayStr);
                    int f = studentDao.updateBir(id, birthday);
                    System.out.println(f>0?"修改生日成功":"修改生日失败");
                    break;
                case 6:
                    System.out.println("6、 删除学生记录");
                    System.out.println("请输入学生学号：");
                    id = sc.next();
                    int fl = studentDao.deleteStu(id);
                    System.out.println(fl>0?"删除成功":"删除失败");
                    break;
            }

        }


    }


    public static void init(){
        System.out.println("===================请选择操作键================\n" +
                "1、 统计学生人数\n" +
                "2、 查看学生名单\n" +
                "3、 按学号查询学生姓名\n" +
                "4、 按姓名查询学生信息\n" +
                "5、 修改学生出生日期\n" +
                "6、 删除学生记录\n" +
                "0、 退出");
    }

}
