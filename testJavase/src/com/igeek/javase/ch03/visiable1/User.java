package com.igeek.javase.ch03.visiable1;

/**
 * @author wangjin
 * 2023/7/12 15:21
 * @description TODO
 */
public class User {
    private String name;
    private int age;
    private char gender;

    //name
    // getter获取姓名
    public String getName(){
        return this.name;
    }
    // setter设置姓名
    public void setName(String name){
        this.name = name;
    }

    //age
    public int getAge(){
        return this.age;
    }
    public void setAge(int age){
        if(age>=1 && age<=120){
            this.age = age;
        }else{
            System.out.println("年龄设置非法");
            this.age = 0;
        }
    }

    //gender
    //getter获取性别
    public char getGender(){return this.gender;}
    //setter设置性别
    public void setGender(char gender) {
        if (gender=='男'||gender=='女'){
            this.gender = gender;
        }else{
            System.out.println("性别设置非法");
            this.gender = '\u0000';
        }

    }


    public void work(){
        System.out.println(+age+"岁"+gender+name+"在上班~");
    }
}
