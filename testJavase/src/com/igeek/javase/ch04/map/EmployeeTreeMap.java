package com.igeek.javase.ch04.map;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author wangjin
 * 2023/7/17 18:38
 * @description TODO
 * TreeMap
 * 1.添加的元素具备比较性，例如Employee implements Comparable<Integer>，重写compareTo()
 * 2.容器具备比较性，new TreeMap(Comparator比较器)，重写compare()
 *
 * 作业：
 * User  name , age
 * 按照姓名的升序进行排列，姓名相同则按照年龄的降序进行排序，都姓名和年龄都相同，则不进行存储
 */
public class EmployeeTreeMap {

    public static void main(String[] args) {
        TreeMap<Employee , Double> map = new TreeMap<>(new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                //按照薪资的升序进行排列，薪资相同则按照名字的降序排列
                if(o1.getSalary() == o2.getSalary()){
                    return o2.getName().compareTo(o1.getName());
                }
                return Double.compare(o1.getSalary() , o2.getSalary());
            }
        });

        Employee e1 = new Employee("zhangsan" , 8800.0);
        Employee e2 = new Employee("lisi" , 6600.0);
        Employee e3 = new Employee("zhanglong" , 8500.0);
        Employee e4 = new Employee("zhaohu" , 10000.0);
        Employee e5 = new Employee("lihu" , 6600.0);
        Employee e6 = new Employee("zhangsan" , 8800.0);

        //ClassCastException 类型转换异常，Employee --》 Comparable比较器
        map.put(e1 , e1.getSalary());
        map.put(e2 , e2.getSalary());
        map.put(e3 , e3.getSalary());
        map.put(e4 , e4.getSalary());
        map.put(e5 , e5.getSalary());
        map.put(e6 , e6.getSalary());

        //迭代元素
        Set<Employee> keys = map.keySet();
        for (Employee key : keys) {
            Double value = map.get(key);
            System.out.println("key = "+key+" , value = "+value);
        }
    }

}
