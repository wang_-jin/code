package com.igeek.demo.ch02.servlet;

import com.igeek.demo.ch02.dao.UserDao;
import com.igeek.demo.ch02.service.UserService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "UserServlet", value = "/UserServlet")
public class UserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("doGet");
        this.doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("doPost");
        //1.处理编码集
        // 请求request中文友好
        request.setCharacterEncoding("UTF-8");
        // 响应response中文友好  有可能会导致css样式丢失
        response.setContentType("text/html;charset=UTF-8");
        // 响应response中文友好  针对于响应json数据
        // response.setCharacterEncoding("UTF-8");

        //2.接收请求参数
        //表单中name="username"  username作为key接收数据
        String username = request.getParameter("username");
        System.out.println("username = " + username);
        String address = request.getParameter("address");
        System.out.println("address = " + address);

        UserService userService = new UserService();
        boolean flag = false;

            flag = userService.userLogin();


        if (flag) {
            //跳转至成功界面
            request.getRequestDispatcher("success.jsp").forward(request,response);
            //response.sendRedirect("success.jsp");
        } else {
            //跳转至失败界面
            response.sendRedirect("error.jsp");
        }


    }
}
