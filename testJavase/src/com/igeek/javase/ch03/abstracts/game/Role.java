package com.igeek.javase.ch03.abstracts.game;

/**
 * @author wangjin
 * 2023/7/13 21:28
 * @description 角色类
 */
public abstract class Role {
    private String nickName;
    private int level;
    private int blood;
    private int power;
    private int defence;
    private String kind;

    public Role() {
    }

    public Role(String nickName, int level, int blood, int power, int defence, String kind) {
        this.nickName = nickName;
        this.level = level;
        this.blood = blood;
        this.power = power;
        this.defence = defence;
        this.kind = kind;
    }
    //基本攻击方法  多态参数Role
    public abstract void attack(Role role);

    //死亡判定方法
    public boolean isDead(){
        if(this.getBlood()<=0){
            System.out.println(getNickName()+"已死亡~");
            return true;
        }
        return false;
    }

    //获取怪兽信息的方法
    public void show(){
        System.out.println(nickName+"的昵称："+this.getNickName());
        System.out.println(nickName+"的等级："+this.getLevel());
        System.out.println(nickName+"的血量："+this.getBlood());
        System.out.println(nickName+"的攻击力："+this.getPower());
        System.out.println(nickName+"的防御力："+this.getDefence());
        System.out.println(nickName+"的类别："+this.getKind());
    }


    /**
     * 获取
     * @return nickName
     */
    public String getNickName() {
        return nickName;
    }

    /**
     * 设置
     * @param nickName
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /**
     * 获取
     * @return level
     */
    public int getLevel() {
        return level;
    }

    /**
     * 设置
     * @param level
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * 获取
     * @return blood
     */
    public int getBlood() {
        return blood;
    }

    /**
     * 设置
     * @param blood
     */
    public void setBlood(int blood) {
        this.blood = blood;
    }

    /**
     * 获取
     * @return power
     */
    public int getPower() {
        return power;
    }

    /**
     * 设置
     * @param power
     */
    public void setPower(int power) {
        this.power = power;
    }

    /**
     * 获取
     * @return defence
     */
    public int getDefence() {
        return defence;
    }

    /**
     * 设置
     * @param defence
     */
    public void setDefence(int defence) {
        this.defence = defence;
    }

    /**
     * 获取
     * @return kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * 设置
     * @param kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    public String toString() {
        return "Role{nickName = " + nickName + ", level = " + level + ", blood = " + blood + ", power = " + power + ", defence = " + defence + ", kind = " + kind + "}";
    }
}
