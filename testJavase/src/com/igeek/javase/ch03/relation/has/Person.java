package com.igeek.javase.ch03.relation.has;

/**
 * @author wangjin
 * 2023/7/13 12:33
 * @description TODO
 * 关联关系
 * 1.语法：一个类作为另外一个类的属性的类型存在
 * 2.has-a
 */
public class Person {
    private String name;
    private int age;

    //一个人有一个手机   关联关系：一对一  单向关联  弱关联
    private Phone phone;

    public void call() {
        System.out.println(this.name + "用" + phone.getLabel() + "拨出电话~");
    }

    public Person() {
    }

    public Person(String name, int age, Phone phone) {
        this.name = name;
        this.age = age;
        this.phone = phone;
    }

    /**
     * 获取
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     *
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     *
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取
     *
     * @return phone
     */
    public Phone getPhone() {
        return phone;
    }

    /**
     * 设置
     *
     * @param phone
     */
    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public String toString() {
        return "Person{name = " + name + ", age = " + age + ", phone = " + phone + "}";
    }

    public static void main(String[] args) {
        Phone phone = new Phone("苹果", "紫色", 9888.0);
        Person p = new Person("李四", 30, phone);
        p.call();

    }
}
