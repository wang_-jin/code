package com.igeek.javase.ch04.list.test;

import com.igeek.javase.ch03.extendss.A;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/7/17 11:08
 * @description TODO
/*
 * 1.List作业
模拟登陆，编写用户类，测试类。
1>.给集合中存储5个用户对象。
2>.从控制台输入用户名和密码，和集合中的对象信息进行比较，
相同表示成功，不同表示失败。
3>.有三次输入机会。
 */
public class Test {
    public static void main(String[] args) {
        ArrayList<User> userList = new ArrayList<>();
        //1>.给集合中存储5个用户对象。
        userList.add(new User("张三" ,"1231"));
        userList.add(new User("李四" ,"1232"));
        userList.add(new User("王五" ,"1236"));
        userList.add(new User("赵六" ,"1235"));
        userList.add(new User("钱七" ,"1234"));
        System.out.println(userList);
        //2>.从控制台输入用户名和密码，和集合中的对象信息进行比较，相同表示成功，不同表示失败。
        boolean flag = false;
        //控制循环次数为3次
        Scanner sc = new Scanner(System.in);
        A:for (int i = 0; i < 3; i++) {
            //控制台输入用户名和密码
            System.out.println("请输入姓名：");
            String username = sc.next();
            System.out.println("请输入密码：");
            String password = sc.next();
            //控制集合元素循环
            B:for(User user : userList){
                //和集合中的对象信息进行比较  NullPointerException空指针异常
                if(user!=null  &&  user.getUserName().equals(username) && user.getPassword().equals(password)){
                    flag=true;
                    System.out.println("登录成功！");
                    break A;
                }
            }
            System.out.println("登陆失败！请重试");
        }
        if (flag==false) {
            System.out.println("三次机会已用完，请下次重试！");
        }
    }
}
