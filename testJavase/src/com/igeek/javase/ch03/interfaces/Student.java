package com.igeek.javase.ch03.interfaces;

/**
 * @author chenmin
 * 2023/7/14 11:14
 * @description TODO
 *
 * 类 实现 接口
 * 1.implements实现  允许多实现
 * 2.一旦实现接口，要么实现其中所有抽象方法，要么自身变成抽象类
 * 3.extends 、 implements同时存在  extends Person implements Shengwu , Animal
 */
public /*abstract*/ class Student extends Person implements Shengwu , Animal {
    @Override
    public void eat() {

    }

    @Override
    public void breath() {

    }
}