package com.igeek.javase.ch03.extendss;

/**
 * @author wangjin
 * 2023/7/12 17:39
 * @description TODO
 * 解耦
 */
public class Teacher extends Person {

    //属性
    private String major;

    //Alt + Insert 弹出弹框
    public Teacher() {
    }

    public Teacher(String name, int age, char gender, String major) {
        //super() 调用父类的构造方法
        super(name, age, gender);
        this.major = major;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    //方法
    public void sleep(){
        System.out.println(this.getAge()+"正在睡觉~");
    }
    public void teach(){
        System.out.println(this.getName()+"正在授课~");
    }
}
