package com.igeek.javase.ch02;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/7/11 15:27
 * @description TODO
 */
public class WhileDemo {
    public static void main(String[] args) {
        System.out.println("和："+ add());
        print();
    }

    /*
     * 1、需求：编写从1-100之间的奇数的和，显示结果。（while）
     */
    public static int add(){
        int sum = 0;
        int i = 1;
        while (i<=100){
            if(i%2!=0){
                sum+=i;
            }
            i++;
        }
        return sum;
    }

    /*
     * 2、需求：接受键盘的输入,如果输入的exit就退出，否则提示用户继续输入!（while）
     */
    public static void print(){
        System.out.println("请输入：");
        Scanner sc = new Scanner(System.in);
        String s = sc.next();
        while (s!=null && !s.equals("")){
            if(s.equals("exit")){
                System.out.println("886");
                System.exit(0); //退出当前系统
            }

            System.out.println("s = "+s);
            s = sc.next();
        }
    }


}
