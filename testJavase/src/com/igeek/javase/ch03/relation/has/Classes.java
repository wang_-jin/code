package com.igeek.javase.ch03.relation.has;

/**
 * @author wangjin
 * 2023/7/13 12:33
 * @description TODO
 *  * int[] arr
 *  * Student[] stus
 */
public class Classes {
    private int no;
    private String className;

    //一个班级中会有多个学生  关联关系：一对多  双向关联  强关联
    private Student[] stus = new Student[10];


    public Classes() {
    }

    public Classes(int no, String className) {
        this.no = no;
        this.className = className;
    }

    public Classes(int no, String className, Student[] stus) {
        this.no = no;
        this.className = className;
        this.stus = stus;
    }

    /**
     * 获取
     * @return no
     */
    public int getNo() {
        return no;
    }

    /**
     * 设置
     * @param no
     */
    public void setNo(int no) {
        this.no = no;
    }

    /**
     * 获取
     * @return className
     */
    public String getClassName() {
        return className;
    }

    /**
     * 设置
     * @param className
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * 获取
     * @return stus
     */
    public Student[] getStus() {
        return stus;
    }

    /**
     * 设置
     * @param stus
     */
    public void setStus(Student[] stus) {
        this.stus = stus;
    }

    /*public String toString() {
        return "Classes{no = " + no + ", className = " + className + ", stus = " + stus + "}";
    }*/

    public void showClasses(){
        System.out.println("班级编号："+this.no);
        System.out.println("班级名称："+this.className);
        System.out.println("=========班级中的学生信息=========");
        if(stus!=null){
            for (int i = 0; i < stus.length; i++) {
                Student stu = this.stus[i];
                if(stu!=null){
                    stu.show();
                }
            }
        }
    }
}
