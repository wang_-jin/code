package com.igeek.javase.ch05.jdbc.dao;

import com.igeek.javase.ch05.jdbc.entity.Department;
import com.igeek.javase.ch05.jdbc.entity.Girl;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.SimpleFormatter;

import static org.junit.Assert.*;

public class GirlsDaoTest {
    private GirlsDao girlsDao;

    @Before
    public void setUp() throws Exception {
        girlsDao = new GirlsDao();
    }

    @Test
    public void insertGirl() throws SQLException, ParseException {
        String str = "2000-02-03";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date borndate = sdf.parse(str);
        Girl girl = new Girl("小明", borndate ,"12311122223",6);
        int i = girlsDao.insertGirl(girl);
        System.out.println(i>0?"插入成功":"插入失败");
    }
    @Test
    public void deleteGirl() throws SQLException {
        int i = girlsDao.deleteGirl(14);
        System.out.println(i > 0 ? "删除成功" : "删除失败");
    }

    @Test
    public void findGirlByPhone() throws SQLException {
        List<Girl> list = girlsDao.findGirlByPhone("12311122223");
        System.out.println(list);
        System.out.println(list.size());
    }
    @Test
    public void selectCount() throws SQLException {
        int count = girlsDao.selectCount("小");
        System.out.println(count);

    }

}