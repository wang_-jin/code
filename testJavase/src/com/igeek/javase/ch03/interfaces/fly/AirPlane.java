package com.igeek.javase.ch03.interfaces.fly;

/**
 * @author wangjin
 * 2023/7/14 14:51
 * @description TODO
 */
public class AirPlane implements CanFly{

    @Override
    public void fly() {
        System.out.println("飞机飞了~");
    }
}
