package com.igeek.javase.ch06.stu.dao;

import com.igeek.javase.ch06.stu.entity.Admin;
import com.igeek.javase.ch06.stu.utils.JDBCUtilsByC3P0;

import java.sql.SQLException;

/**
 * @author wangjin
 * 2023/7/20 21:56
 * @description TODO
 */
public class AdminDao extends BaseDao<Admin>{
    public boolean login(String username , String password) throws SQLException {
        String sql = "select count(*) from admin where username = ? and password = ?";
        Long count = (Long) this.selectSingleValue(JDBCUtilsByC3P0.getConn() , sql , username , password);
        return count > 0;
    }
}
