package com.igeek.javase.ch03.relation.use;

/**
 * @author wangjin
 * 2023/7/13 17:46
 * @description TODO
 */
public class Car {
    private String label;
    private String color;

    public void run(){
        System.out.println("一辆"+label+"正在行驶着~");
    }

    public Car() {
    }

    public Car(String label, String color) {
        this.label = label;
        this.color = color;
    }

    /**
     * 获取
     * @return label
     */
    public String getLabel() {
        return label;
    }

    /**
     * 设置
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * 获取
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * 设置
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }

    public String toString() {
        return "Car{label = " + label + ", color = " + color + "}";
    }
}
