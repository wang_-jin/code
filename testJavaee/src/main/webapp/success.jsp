<%--
  Created by IntelliJ IDEA.
  User: 86158
  Date: 2023/7/25
  Time: 16:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>success.jsp</title>
</head>
<body>
    <%--  ${param.username}  EL表达式 ${key}  param获取请求参数中的数据 --%>
    <h1 style="color: green">登录成功，欢迎 ${param.username}</h1>
</body>
</html>
