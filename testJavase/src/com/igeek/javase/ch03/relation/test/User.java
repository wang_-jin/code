package com.igeek.javase.ch03.relation.test;

/**
 * @author wangjin
 * 2023/7/13 17:57
 * @description TODO
 *  一.订单和货物，用户：
 * 1.一个订单会有几个货物（一对多的关系）,订单中一个方法（查看商品详情信息）
 * 2.货物是订单的属性
 * 3.订单与用户之间的关系，用户类中有下单的方法（用户姓名和订单号）
 * 4.测试类（调用执行下单方法和查看商品详情信息的方法）
 * 备注：用户User、订单Order、货物Goods
 */
public class User {
    private String name;
    private String phonenum;
    private String address;

    private Order[] orders = new Order[10];


    public User() {
    }
    public User(String name, String phonenum, String address) {
        this.name = name;
        this.phonenum = phonenum;
        this.address = address;
    }

    public User(String name, String phonenum, String address, Order[] orders) {
        this.name = name;
        this.phonenum = phonenum;
        this.address = address;
        this.orders = orders;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return phonenum
     */
    public String getPhonenum() {
        return phonenum;
    }

    /**
     * 设置
     * @param phonenum
     */
    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum;
    }

    /**
     * 获取
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 获取
     * @return orders
     */
    public Order[] getOrders() {
        return orders;
    }

    /**
     * 设置
     * @param orders
     */
    public void setOrders(Order[] orders) {
        this.orders = orders;
    }

    public void showbuy(){
        System.out.println("用户姓名："+this.name);
        if (orders!=null){
            for (int i = 0; i < orders.length; i++) {
                Order order = this.orders[i];
                if (order!=null){
                    System.out.println("订单编号："+order.getNo());
                }
            }

        }
        System.out.println("========订单详情========");
    }
}

