package com.igeek.javase.ch05.jdbc.dao;

import com.igeek.javase.ch05.jdbc.entity.Girl;
import com.igeek.javase.ch05.jdbc.utils.JDBCUtils;
import com.igeek.javase.ch05.jdbc.utils.JDBCUtilsByC3P0;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author chenmin
 * 2023/7/19 14:54
 * @description 数据交互层
 *
 * JDBC操作步骤
 * 1.加载驱动类
 * 2.获取连接对象
 * 3.编写sql，预编译，获取语句对象
 * 4.处理?占位符
 * 5.执行查询，获取结果集
 * 6.处理结果集
 * 7.关闭资源
 */
public class GirlDao {

    //1.通过ID查询女生信息
    public Girl selectOneById(int id) throws ClassNotFoundException, SQLException {
        //1.加载驱动类
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2.获取连接对象
        Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/testdql?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai" ,
                "root" , "root");
        //3.编写sql，预编译，获取语句对象
        String sql = "select * from girl where id = ?";
        PreparedStatement ppst = connection.prepareStatement(sql);
        //4.处理?占位符  第一个参数：?的位置  第二个参数：数据
        ppst.setInt(1 , id);
        //5.执行查询，获取结果集
        ResultSet rs = ppst.executeQuery();
        //6.处理结果集
        Girl girl = null;
        while (rs.next()){
            int ids = rs.getInt("id");
            String name = rs.getString("name");
            String sex = rs.getString("sex");
            Date borndate = rs.getDate("borndate");
            String phone = rs.getString("phone");
            int boyfriendId = rs.getInt("boyfriend_id");
            girl = new Girl(ids , name , sex , borndate , phone , null , boyfriendId);
        }
        //7.关闭资源
        if(rs!=null){
            rs.close();
        }
        if(ppst!=null){
            ppst.close();
        }
        if(connection!=null){
            connection.close();
        }
        return girl;
    }

    //2.根据名字模糊查询所有女生信息
    public List<Girl> selectAllByName(String name) throws SQLException {
        List<Girl> list = new ArrayList<>();
        //1.获取连接对象
        Connection conn = JDBCUtilsByC3P0.getConn();
        //2.获取语句对象
        String sql = "select * from girl where name like concat('%',?,'%')";
        PreparedStatement ppst = conn.prepareStatement(sql);
        //3.给?占位符传值
        ppst.setString(1 , name);
        //4.执行查询
        ResultSet rs = ppst.executeQuery();
        //5.处理结果集
        while (rs.next()){
            int ids = rs.getInt("id");
            String names = rs.getString("name");
            String sex = rs.getString("sex");
            Date borndate = rs.getDate("borndate");
            String phone = rs.getString("phone");
            int boyfriendId = rs.getInt("boyfriend_id");
            Girl girl = new Girl(ids , names , sex , borndate , phone , null , boyfriendId);
            list.add(girl);
        }
        //6.关闭资源
        JDBCUtils.close(conn , ppst , rs);
        return list;
    }

    //4.根据女生的姓名更新手机号
    public int updateGirl(String name , String phone) throws SQLException {
        //1.获取连接对象
        Connection conn = JDBCUtilsByC3P0.getConn();
        //2.获取语句对象
        String sql = "update girl set phone = ? where name = ?";
        PreparedStatement ppst = conn.prepareStatement(sql);
        //3.处理?占位符
        ppst.setString(1 , phone);
        ppst.setString(2 , name);
        //4.执行更新
        int i = ppst.executeUpdate();
        //5.关闭资源
        JDBCUtilsByC3P0.close(conn , ppst , null);
        return i;
    }


    //3.添加女生信息
    public int insertGirl(int id , String name , String sex , Date borndate , String phone , int boyfriendId) throws SQLException {
        //1.获取连接对象
        Connection conn = JDBCUtilsByC3P0.getConn();
        //2.获取语句对象
        String sql = "insert ignore into girl values(?,?,?,?,?,null,?) ";
        PreparedStatement ppst = conn.prepareStatement(sql);
        //3.处理?占位符
        ppst.setInt(1, id);
        ppst.setString(2, name);
        ppst.setString(3, sex);
        ppst.setDate(4, borndate);
        ppst.setString(5, phone);
        ppst.setInt(6, boyfriendId);
        //4.执行更新
        int i = ppst.executeUpdate();
        //5.关闭资源
        JDBCUtilsByC3P0.close(conn, ppst, null);
        return i;
    }

    //5.通过ID删除女生信息
    public int deleteOneById(int id) throws  SQLException {
        //1.加载驱动类
        //Class.forName("com.mysql.cj.jdbc.Driver");
        //2.获取连接对象
        Connection conn = JDBCUtilsByC3P0.getConn();
        //3.编写sql，预编译，获取语句对象
        String sql = "delete from girl where id = ?";
        PreparedStatement ppst = conn.prepareStatement(sql);
        //4.处理?占位符  第一个参数：?的位置  第二个参数：数据
        ppst.setInt( 1, id);
        //5.执行查询，获取结果集
        int i = ppst.executeUpdate();

        JDBCUtilsByC3P0.close(conn , ppst , null);
        return i;

    }

    //6.根据手机号查询女孩信息
    public Girl selectOneByPhone(String phone) throws ClassNotFoundException, SQLException {
        /*1.加载驱动类
        Class.forName("com.mysql.cj.jdbc.Driver");*/
        //2.获取连接对象
        Connection conn = JDBCUtilsByC3P0.getConn();
        //3.编写sql，预编译，获取语句对象
        String sql = "select * from girl where phone = ?";
        PreparedStatement ppst = conn.prepareStatement(sql);
        //4.处理?占位符  第一个参数：?的位置  第二个参数：数据
        ppst.setString( 1, phone);
        //5.执行查询，获取结果集
        ResultSet rs = ppst.executeQuery();
        //6.处理结果集
        Girl girl = null;
        while (rs.next()){
            int id = rs.getInt("id");
            String name = rs.getString("name");
            String sex = rs.getString("sex");
            Date borndate = rs.getDate("borndate");
            String phones = rs.getString("phone");
            int boyfriendId = rs.getInt("boyfriend_id");
            girl = new Girl(id , name , sex , borndate , phones , null , boyfriendId);
        }
        //5.关闭资源
        JDBCUtilsByC3P0.close(conn , ppst , null);
        return girl;

    }
}

