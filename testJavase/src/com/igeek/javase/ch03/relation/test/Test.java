package com.igeek.javase.ch03.relation.test;

/**
 * @author wangjin
 * 2023/7/13 20:06
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        User user = new User("张三","123456789","北京三里屯xx路xx号");



        Goods goods1 = new Goods("苹果手机",8000,"最新款",2);
        Goods goods2 = new Goods("华为手机",5000,"热卖款",2);
        Goods goods3 = new Goods("小米手机",4000,"旗舰版",1);
        Goods[] good1 ={goods1,goods3};
        Goods[] good2 ={goods2};

        Order order1 = new Order("001",user,good1);
        Order order2 = new Order("002",user,good2);
        Order order3 = new Order("003",user,good1);
        Order[] orders ={order1,order3};

        user.setOrders(orders);
        user.showbuy();
        order1.showorder();
        System.out.println("--------------------------------");
        order3.showorder();





    }
}
