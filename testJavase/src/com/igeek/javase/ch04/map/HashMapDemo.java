package com.igeek.javase.ch04.map;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author wangjin
 * 2023/7/17 18:37
 * @description TODO
 * HashMap
 * 1.以键值对存放数据  put
 * 2.存数据的顺序，与取数据顺序，不一致
 * 3.存null键、null值
 * 4.一旦键重复，值会发生覆盖
 */
public class HashMapDemo {

    public static void main(String[] args) {
        //key键：字符串  value值：整型
        HashMap<String,Integer> map = new HashMap<>();

        //添加元素
        map.put("abd",20);
        map.put(null,20);
        map.put("abc",10);
        map.put("abc",null);
        map.put("abb",30);

        //输出
        System.out.println(map);

        System.out.println("--------------Map迭代---------------");
        //迭代Entry Key-Value
        Set<Map.Entry<String, Integer>> entrySet = map.entrySet();
        for (Map.Entry<String, Integer> entry : entrySet){
            System.out.println("键："+entry.getKey());
            System.out.println("值："+entry.getValue());
        }

        //迭代Key
        Set<String> keys = map.keySet();
        for(String key : keys){
            System.out.println("key = "+key +" , value = "+map.get(key));
        }
    }

}

