package com.igeek.javase.fortest;

/**
 * @author wangjin
 * 2023/7/12 0:43
 * 5.有口井7米深，一只青蛙白天爬3米，晚上坠下2米，问这青蛙几天才能爬出这口井(while)
 * @description 青蛙
 */
public class ForTest5 {
    public static void main(String[] args) {
        int num=0;//储存器
        int i=1;
        while(i<100){
            num+=3;//每天爬三米储存到num里面
            if(num>=7){
                System.out.println("青蛙需要爬出井的天数："+i);
                break;
            }
            num-=2;//晚上下坠2米,从num减去2米
            i++;
        }
    }
}
