package com.igeek.javase.ch03.interfaces.storage;

/**
 * @author wangjin
 * 2023/7/14 15:38
 * @description TODO
 */
public class UPan implements IStorage{
    @Override
    public void read() {
        System.out.println("U盘开始读数据");
    }

    @Override
    public void write() {
        System.out.println("U盘开始写数据");
    }
    public void UPanWork(){
        System.out.println("U盘在工作");
    }
}
