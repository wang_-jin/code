package com.igeek.javase.ch03.extendss.test;

/**
 * @author wangjin
 * 2023/7/13 13:29
 * @description TODO
 * 2.现有员工Employee类，有姓名，地址，基本工资属性，
 * 董事类和经理类除以上属性，董事类有交通补助，经理类有所管部门，
 * 三个类中都有显示其基本信息的方法show，但其实现方式不同，
 * 请写出各类构造方法，及属性的get，set方法。
 * 并要用到this和super关键字。
 */
public class Employee {
    private String name;
    private String address;
    private double salary;


    public Employee() {
    }

    public Employee(String name, String address, double salary) {
        this.name = name;
        this.address = address;
        this.salary = salary;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 获取
     * @return salary
     */
    public double getSalary() {
        return salary;
    }

    /**
     * 设置
     * @param salary
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String toString() {
        return "Employee{name = " + name + ", address = " + address + ", salary = " + salary + "}";
    }
}
