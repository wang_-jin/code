package com.igeek.javase.ch03.relation.game;

/**
 * @author wangjin
 * 2023/7/13 17:47
 * @description TODO
 */
public class Monster extends Role{


    //基本攻击方法
    public void attack(Hero h){
        //英雄的失血量 = 怪兽的攻击力 - 英雄的防御力
        int blood = this.getPower() - h.getDefence();
        if(blood>=0){
            h.setBlood(h.getBlood() - blood);
            System.out.println("英雄本回合失血量："+blood+"，剩余血量："+h.getBlood());
        }else{
            System.out.println("此回合攻击无效");
        }
    }

    //死亡判定方法
    public boolean isDead(){
        if(this.getBlood()<=0){
            System.out.println("当前怪兽已死亡~");
            return true;
        }
        return false;
    }

    //获取怪兽信息的方法
    public void show(){
        System.out.println("怪兽的昵称："+this.getNickName());
        System.out.println("怪兽的等级："+this.getLevel());
        System.out.println("怪兽的血量："+this.getBlood());
        System.out.println("怪兽的攻击力："+this.getPower());
        System.out.println("怪兽的防御力："+this.getDefence());
        System.out.println("怪兽的类别："+this.getKind());
    }

    public Monster() {
    }

    public Monster(String nickName, int level, int blood, int power, int defence, String kind) {
        super(nickName,level,blood,power,defence,kind);
    }

    @Override
    public String toString() {
        return "Monster{} " + super.toString();
    }
}
