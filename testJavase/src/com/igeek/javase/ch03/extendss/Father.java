package com.igeek.javase.ch03.extendss;

/**
 * @author wangjin
 * 2023/7/13 10:32
 * @description TODO
 */
public class Father extends GrandFather{

    //职业
    private String occupation;

    /**
     * 方法重写   ctrl + o
     * @Override注解，编译检查，一旦不符合方法重写的规则，则直接编译报错
     * 1.方法重写，发生在继承中，子类重写父类的方法，方法名必须一致
     * 2.形参列表必须一致
     * 3.方法的返回值，若是基本数据类型则一致；若是引用数据类型则子类<=父类
     * 4.访问权限修饰符，子类>=父类   public>protected>(default)>private
     * 5.抛出异常，子类<=父类   Exception > RuntimeException > NullPointerException
     */
    @Override
    public Father show(GrandFather a) /*throws Exception*/ {
        //super.show();  执行父类的方法
        System.out.println("Father的姓名："+this.getName());
        System.out.println("Father的年龄："+this.getAge());
        System.out.println("Father的职业："+occupation);
        return null;
    }

    public Father() {
    }

    public Father(String name, int age, String occupation) {
        super(name, age);
        this.occupation = occupation;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
}
