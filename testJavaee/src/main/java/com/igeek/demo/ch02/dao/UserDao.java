package com.igeek.demo.ch02.dao;


import com.igeek.demo.ch02.entity.User;
import com.igeek.demo.ch02.utils.JDBCUtilsByC3P0;

import java.sql.SQLException;

/**
 * @author chenmin
 * 2023/7/25 15:53
 * @description TODO
 */
public class UserDao extends BaseDao<User>{
    public boolean login(String username , String address) throws SQLException {
        String sql = "select count(*) from user where username = ? and address = ?";
        Long count = (Long) this.selectSingleValue(JDBCUtilsByC3P0.getConn() , sql , username , address);
        return count > 0;
    }
}
