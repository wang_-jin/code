package com.igeek.javase.ch04.map;

import java.util.Objects;

/**
 * @author wangjin
 * 2023/7/17 18:37
 * @description TODO
 */
public class Student {

    private String name;
    private int age;
    private int level;

    public Student() {
    }

    public Student(String name, int age, int level) {
        this.name = name;
        this.age = age;
        this.level = level;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取
     * @return level
     */
    public int getLevel() {
        return level;
    }

    /**
     * 设置
     * @param level
     */
    public void setLevel(int level) {
        this.level = level;
    }

    //重写equals()和hashCode()
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return age == student.age && level == student.level && Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, level);
    }

    /*public String toString() {
        return "Student{name = " + name + ", age = " + age + ", level = " + level + "}";
    }*/
}