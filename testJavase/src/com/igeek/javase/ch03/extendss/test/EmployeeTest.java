package com.igeek.javase.ch03.extendss.test;

/**
 * @author wangjin
 * 2023/7/13 13:30
 * @description TODO
 */
public class EmployeeTest {
    public static void main(String[] args) {
        Employee  employee1 = new Employee("张三","北京",10000);
        Employee  employee2 = new Boss("李四","上海",100000,50000);
        Employee  employee3 = new Manager("王五","深圳",15000,"技术部");

        System.out.println("Employee"+employee1);
        System.out.println("Employee"+employee2);
        System.out.println("Employee"+employee3);


    }
}
