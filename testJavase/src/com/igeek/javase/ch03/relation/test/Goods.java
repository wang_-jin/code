package com.igeek.javase.ch03.relation.test;

/**
 * @author wangjin
 * 2023/7/13 17:58
 * @description TODO
 *  一.订单和货物，用户：
 * 1.一个订单会有几个货物（一对多的关系）,订单中一个方法（查看商品详情信息）
 * 2.货物是订单的属性
 * 3.订单与用户之间的关系，用户类中有下单的方法（用户姓名和订单号）
 * 4.测试类（调用执行下单方法和查看商品详情信息的方法）
 * 备注：用户User、订单Order、货物Goods
 */
public class Goods {
    private String goodsname;
    private int price;
    private String description;
    private int num;


    public Goods() {
    }


    public Goods(String goodsname, int price, String description, int num) {
        this.goodsname = goodsname;
        this.price = price;
        this.description = description;
        this.num = num;
    }





    /**
     * 获取
     * @return goodsname
     */
    public String getGoodsname() {
        return goodsname;
    }

    /**
     * 设置
     * @param goodsname
     */
    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    /**
     * 获取
     * @return price
     */
    public int getPrice() {
        return price;
    }

    /**
     * 设置
     * @param price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * 获取
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取
     * @return num
     */
    public int getNum() {
        return num;
    }

    /**
     * 设置
     * @param num
     */
    public void setNum(int num) {
        this.num = num;
    }

    public void show(){
        System.out.println("商品名称："+this.goodsname);
        System.out.println("商品价格："+this.price);
        System.out.println("商品描述："+this.description);
        System.out.println("商品数量："+this.num);

    }
}
