package com.igeek.javase.ch05.jdbc.dao;

import com.igeek.javase.ch05.jdbc.utils.JDBCUtilsByC3P0;
import com.igeek.javase.ch06.stu.entity.Student;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * @author wangjin
 * 2023/7/20 16:19
 * @description 学生管理系统
 */
public class StusDao extends BaseDao<Student> {
//    1 、统计学生人数
    public int selectCount() throws SQLException {
        String sql = "select count(*) from stus ";
        Long count =(Long) this.selectSingleValue(JDBCUtilsByC3P0.getConn(), sql);
        return count.intValue();
    }
//    2、查看学生名单
    public List<Student> selectStus() throws SQLException {
        String sql = "select * from stus ";
        List<Student> list = this.selectAll(JDBCUtilsByC3P0.getConn() , sql , Student.class );
        return list;
    }
//    3、按学号查询学生姓名
    public Student selectOneById(String id) throws SQLException {
        String sql = "select * from stus where id = ?";
        Student student = selectOne(JDBCUtilsByC3P0.getConn() , sql , Student.class , id);
        return student;
    }
//    4、按姓名查询学生信息
    public List<Student> selectOneByName(String name) throws SQLException {
    String sql = "select * from stus where name like concat('%' , ? , '%')";
    List<Student> list = selectAll(JDBCUtilsByC3P0.getConn() , sql , Student.class , name);
    return list;
    }
//    5、修改学生出生日期
    public int updateBir(Date birthday , String name) throws SQLException {
        String sql = "update stus set birthday = ? where name = ?";
        int i = update(JDBCUtilsByC3P0.getConn() , sql , birthday , name);
        return  i;
    }

//    6、删除学生记录
    public int deleteStu(String id) throws SQLException {
        String sql = "delete from stus where id = ?";
        int i = update(JDBCUtilsByC3P0.getConn(), sql, id);
        return i;
    }
//    0、退出

}
