package com.igeek.javase.ch04;

/**
 * @author wangjin
 * 2023/7/17 0:19
 * @description TODO
 * 泛型类
 * 1.E T K V  任意大写字母
 * 2.public class 类<任意大写字母>{}
 * 3.数据类型，但是目前未可知
 *      3.1 声明变量
 *      3.2 声明方法，方法的返回值类型
 *      3.3 声明方法，方法的形参列表的类型
 * 4.泛型，是引用数据类型
 * Person,String,Integer,Character,Double,Boolean....
 * 5.可以在声明类时，<>编写多个任意大写字母
 * 6.静态方法中不适合使用泛型
 *
 * 泛型使用场景
 * 1.泛型多数用于集合中 List<String>
 * 2.工具类 XxxUtils<T> ， 更适用于在抽象类、接口中，方便拓展
 */
public class GenDemo<E , A , T> {

    private E name;
    private A age;
    private T gender;

    public GenDemo(E name, A age, T gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

   /* public E getName(){
        return name;
    }*/

    /*public void setName(E name){
        this.name = name;
    }*/

    public static void main(String[] args) {
        GenDemo<String , Integer , Character> gen = new GenDemo<>("张三" , 18 , '女');
        System.out.println(gen.name);
        System.out.println(gen.age);
        System.out.println(gen.gender);
    }
}

