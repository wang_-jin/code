package com.igeek.javase.ch03.extendss;

/**
 * @author wangjin
 * 2023/7/12 18:00
 * @description TODO
 * Worker extends Person  个性：work()
 */
public class Worker extends Person{
    private String job;
    public Worker(){

    }

    public Worker(String name, int age, char gender, String job) {
        super(name, age, gender);
        this.job = job;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
    public void work(){
        System.out.println(this.getName()+"正在工作~");
    }
}
