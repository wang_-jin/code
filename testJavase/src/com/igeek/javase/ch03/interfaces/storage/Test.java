package com.igeek.javase.ch03.interfaces.storage;

/**
 * @author wangjin
 * 2023/7/14 15:39
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        IStorage p = new MP3();
        Computer computer = new Computer(p);
        computer.readData();
        computer.writeData();

        if (p instanceof MP3){
            ((MP3) p).MP3Work();
        }




    }
}
