package com.igeek.javase.ch03.extendss;

import com.sun.corba.se.spi.orbutil.threadpool.Work;

/**
 * @author wangjin
 * 2023/7/12 17:39
 * @description TODO
 * 作业：Worker extends Person  个性：work()
 */
public class Test {

    public static void main(String[] args) {
        //人  父类
        Person person = new Person("人" , 18 , '男');
        person.eat();
        person.sleep();
        //子类特有的属性和方法，父类无法使用
        //person.study();
        //person.teach();

        //学生
        Student stu = new Student("李四" , 20 , '男' , 3);
        stu.eat();
        stu.sleep();
        stu.study();

        //教师
        Teacher teacher = new Teacher("老王" , 50 , '男' , "高级工程师");
        teacher.eat();
        teacher.sleep();
        teacher.teach();


        //工人
        Worker worker = new Worker("张三" , 23 , '男' , "医生");
        worker.eat();
        worker.sleep();
        worker.work();


        //多态：父类的引用指向子类的实例
        Person p1 = new Student("小李同学" , 18 , '女' , 2);
        Person p2 = new Teacher("陈老师" , 35 , '女' , "计算机");
        Person p3 = new Worker("王工" , 40 , '女',"教师");



        //调用属性、方法  调用父类中有的
        System.out.println("p2姓名："+p2.getName());

        //编译看左边，运行看右边
        p2.sleep();
        //p2.teach();

        //强制转换  instanceof运算符，判断运行期类型
        if(p1 instanceof Teacher){
            ((Teacher)p1).teach(); //ClassCastException 强制转换异常
        }
        if(p2 instanceof  Teacher){
            ((Teacher)p2).teach();
        }
    }

}