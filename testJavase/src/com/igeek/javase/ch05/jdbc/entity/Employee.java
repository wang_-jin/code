package com.igeek.javase.ch05.jdbc.entity;

import java.util.Date;

/**
 * @author chenmin
 * 2023/7/20 10:36
 * @description TODO
 */
public class Employee {

    private int employee_id;
    private String first_name;
    private String last_name;
    private String email;
    private String phone_number;
    private String job_id;
    private double salary;
    private double commission_pct;
    private int manager_id;
    private int department_id;
    private Date hiredate;
    
    public Employee() {
    }

    public Employee(int employee_id, String first_name, String last_name, String email, String phone_number, String job_id, double salary, double commission_pct, int manager_id, int department_id, Date hiredate) {
        this.employee_id = employee_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.phone_number = phone_number;
        this.job_id = job_id;
        this.salary = salary;
        this.commission_pct = commission_pct;
        this.manager_id = manager_id;
        this.department_id = department_id;
        this.hiredate = hiredate;
    }

    /**
     * 获取
     * @return employee_id
     */
    public int getEmployee_id() {
        return employee_id;
    }

    /**
     * 设置
     * @param employee_id
     */
    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    /**
     * 获取
     * @return first_name
     */
    public String getFirst_name() {
        return first_name;
    }

    /**
     * 设置
     * @param first_name
     */
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    /**
     * 获取
     * @return last_name
     */
    public String getLast_name() {
        return last_name;
    }

    /**
     * 设置
     * @param last_name
     */
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    /**
     * 获取
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取
     * @return phone_number
     */
    public String getPhone_number() {
        return phone_number;
    }

    /**
     * 设置
     * @param phone_number
     */
    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    /**
     * 获取
     * @return job_id
     */
    public String getJob_id() {
        return job_id;
    }

    /**
     * 设置
     * @param job_id
     */
    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    /**
     * 获取
     * @return salary
     */
    public double getSalary() {
        return salary;
    }

    /**
     * 设置
     * @param salary
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    /**
     * 获取
     * @return commission_pct
     */
    public double getCommission_pct() {
        return commission_pct;
    }

    /**
     * 设置
     * @param commission_pct
     */
    public void setCommission_pct(double commission_pct) {
        this.commission_pct = commission_pct;
    }

    /**
     * 获取
     * @return manager_id
     */
    public int getManager_id() {
        return manager_id;
    }

    /**
     * 设置
     * @param manager_id
     */
    public void setManager_id(int manager_id) {
        this.manager_id = manager_id;
    }

    /**
     * 获取
     * @return department_id
     */
    public int getDepartment_id() {
        return department_id;
    }

    /**
     * 设置
     * @param department_id
     */
    public void setDepartment_id(int department_id) {
        this.department_id = department_id;
    }

    /**
     * 获取
     * @return hiredate
     */
    public Date getHiredate() {
        return hiredate;
    }

    /**
     * 设置
     * @param hiredate
     */
    public void setHiredate(Date hiredate) {
        this.hiredate = hiredate;
    }

    public String toString() {
        return "Employee{employee_id = " + employee_id + ", first_name = " + first_name + ", last_name = " + last_name + ", email = " + email + ", phone_number = " + phone_number + ", job_id = " + job_id + ", salary = " + salary + ", commission_pct = " + commission_pct + ", manager_id = " + manager_id + ", department_id = " + department_id + ", hiredate = " + hiredate + "}";
    }
}
