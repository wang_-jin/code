package com.igeek.javase.ch03.obj;

/**
 * @author wangjin
 * 2023/7/12 18:53
 * @description TODO
 * 类：就是一个模板，定义多个对象共同的属性和方法
 * 对象：通过数据 和 行为 进行描述
 *
 * 数据 ----> 属性
 * 行为 ----> 方法
 */
public class Student {
    //属性
    String name;
    int age;
    char gender;

    //无参构造方法  默认提供
    public Student(){

    }

    //有参构造方法  一旦提供，则不再默认提供无参构造方法
    public Student(String name,int age,char gender){
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    //行为
    void study(){
        System.out.println(name+"正在学习~");
    }

    void sleep(){
        System.out.println(age+"岁的同学，正在睡觉~");
    }

    public static void main(String[] args) {
        //类  对象名 = new  构造方法();
        Student stu1 = new Student();
        //调用属性  对象名.属性
        stu1.name = "张三";
        stu1.age = 20;
        stu1.gender = '男';
        //调用方法  对象名.方法()
        stu1.study();
        stu1.sleep();

        System.out.println("----------------");
        //类  对象名 = new  构造方法(值,值,....);
        Student stu2 = new Student("李四" , 22 , '女');
        //调用方法  对象名.方法()
        stu2.study();
        stu2.sleep();
    }
}
