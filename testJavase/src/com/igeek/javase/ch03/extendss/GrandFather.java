package com.igeek.javase.ch03.extendss;

/**
 * @author wangjin
 * 2023/7/13 10:32
 * @description TODO
 */
public class GrandFather {

    private String name;
    private int age;

    GrandFather show(GrandFather a) throws RuntimeException{
        System.out.println("GrandFather的姓名："+name);
        System.out.println("GrandFather的年龄："+age);
        return null;
    }

    public GrandFather() {
    }

    public GrandFather(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
