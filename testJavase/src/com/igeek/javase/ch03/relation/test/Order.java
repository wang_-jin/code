package com.igeek.javase.ch03.relation.test;

/**
 * @author wangjin
 * 2023/7/13 17:58
 * @description TODO
 *  一.订单和货物，用户：
 * 1.一个订单会有几个货物（一对多的关系）,订单中一个方法（查看商品详情信息）
 * 2.货物是订单的属性
 * 3.订单与用户之间的关系，用户类中有下单的方法（用户姓名和订单号）
 * 4.测试类（调用执行下单方法和查看商品详情信息的方法）
 * 备注：用户User、订单Order、货物Goods
 */
public class Order {
    private String no;
    private User user;
    private Goods[] goods = new Goods[10];


    public Order() {
    }

    public Order(String no, User user) {
        this.no = no;
        this.user = user;

    }

    public Order(String no, User user, Goods[] goods) {
        this.no = no;
        this.user = user;
        this.goods = goods;
    }

    /**
     * 获取
     * @return no
     */
    public String getNo() {
        return no;
    }

    /**
     * 设置
     * @param no
     */
    public void setNo(String no) {
        this.no = no;
    }

    /**
     * 获取
     * @return user
     */
    public User getUser() {
        return user;
    }

    /**
     * 设置
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * 获取
     * @return goods
     */
    public Goods[] getGoods() {
        return goods;
    }

    /**
     * 设置
     * @param goods
     */
    public void setGoods(Goods[] goods) {
        this.goods = goods;
    }

    public void showorder(){
        System.out.println("订单编号："+this.getNo());
        System.out.println("用户信息："+user.getName());
        System.out.println("地址信息："+user.getAddress());
        System.out.println("用户手机号："+user.getPhonenum());
        if (goods!=null){
            for (int i = 0; i < goods.length; i++) {
                Goods good = this.goods[i];
                if (good!=null){
                    good.show();
                }
            }
        }
    }
}
