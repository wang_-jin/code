package com.igeek.javase.ch03.interfaces.storage;

/**
 * @author wangjin
 * 2023/7/14 15:38
 * @description TODO
 */
public class Light implements IStorage{
    @Override
    public void read() {
        System.out.println("灯开始读数据");
    }

    @Override
    public void write() {
        System.out.println("灯开始写数据");
    }
    public void lightWork(){
        System.out.println("灯亮了");
    }
}
