package com.igeek.javase.ch04.list;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author wangjin
 * 2023/7/17 10:45
 * @description TODO
 * ArrayList数组扩容的机制？  add
 * 1.new   构建ArrayList对象时，初始长度为0
 * 2.add() 添加元素，检测是否是第一次添加元素
 * 是，则直接给当前数组赋值默认容量10
 * 3.add() 添加元素，检测是否超过原有数组长度
 * 第11次添加，已超过长度10，则进行数组扩容及拷贝
 * grow(minCapacity); 长度15
 * elementData = Arrays.copyOf(elementData, newCapacity);  数组扩容及拷贝
 */
public class ArrayListDemo {
    public static void main(String[] args) {
        //String 泛型类，约束集合中添加的元素类型
        ArrayList<String> list = new ArrayList<>();
        //第1次添加元素
        list.add("abc1");
        list.add("abc2");
        list.add("abc3");
        list.add("hello4");
        list.add("abc5");
        list.add("hello6");
        list.add("abc7");
        list.add("hello8");
        list.add("hello9");
        list.add("abc10");
        //第11次添加元素
        list.add("abc11");
        System.out.println(list);
        System.out.println(list.size());

        //迭代方式
        System.out.println("-------方式一：数组下表-------");
        ArrayList<String> newList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            String s = list.get(i);
            if (s.contains("hello")){
                newList.add(s);
            }
            //下标 0~集合size()-1
            //System.out.println(s);
        }
        System.out.println(list);
        System.out.println("newList = "+newList);

        System.out.println("-------方式二：加强foreach-------");
        //for(集合容器中元素的类型  临时变量名 :  集合名字){}
        for (String str: list) {
            System.out.println("str = "+str);
        }
        System.out.println(list);

        System.out.println("--------- 方式三：迭代器Iterator -------");
        //获取迭代器Iterator
        Iterator<String> iterator = list.iterator();
        //hasNext() 是否有下一个值
        while (iterator.hasNext()){
            //next() 取出
            String string = iterator.next();
            /*if(string.contains("hello")){
                //remove() 移除
                iterator.remove();
            }*/
            //System.out.println("string = "+string);
        }
        System.out.println(list);

        System.out.println("--------- 方式四：Stream流 -------");
        //stream()将集合->流对象  filter()过滤  forEach()迭代  System.out::println方法引用
        list.stream().filter(s->s.contains("hello")).forEach(System.out::println);
    }
}
