package com.igeek.javase.ch04.map;

/**
 * @author wangjin
 * 2023/7/17 18:38
 * @description TODO
 * Map<K,V> 接口
 * 1.键值对  key - value
 * 2.Key键，HashMap的key==>HashSet   TreeMap的key===>TreeSet
 * 3.Value值，Collection集合类型
 * 4.Key不允许重复的
 * 5.不保证存取顺序
 *
 * HashMap
 * 1.以键值对存放数据  put
 * 2.存数据的顺序，与取数据顺序，不一致
 * 3.存null键、null值
 * 4.一旦键重复，值会发生覆盖
 * 5.线程不安全
 *
 * TreeMap
 * 1.以键值对存放数据  put
 * 2.按照key键进行排序
 * 3.不允许存null键
 * 4.一旦键重复，值会发生覆盖
 * 5.线程不安全
 *
 * HashTable
 * 线程安全
 * ConcurrentHashMap
 * 线程安全
 */
public class MapDemo {
}
