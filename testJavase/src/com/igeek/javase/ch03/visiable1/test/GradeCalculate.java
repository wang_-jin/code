package com.igeek.javase.ch03.visiable1.test;

/**
 * @author wangjin
 * 2023/7/13 13:12
 * @description TODO
 */
public class GradeCalculate {
    private float jgrade;
    private float cgrade;
    private float sgrade;

    //java成绩
    public float getJgrade() {return jgrade;}
    public void setJgrade(float jgrade) {this.jgrade = jgrade;}

    //C#成绩
    public float getCgrade() {return cgrade;}
    public void setCgrade(float cgrade) {this.cgrade = cgrade;}

    //数据库成绩
    public float getSgrade() {return sgrade;}
    public void setSgrade(float sgrade) {this.sgrade = sgrade;}

    //无参构造
    public  GradeCalculate(){

    }

    //有参构造
    public GradeCalculate(float jgrade,float cgrade,float sgrade){
        this.jgrade = jgrade;
        this.cgrade = cgrade;
        this.sgrade = sgrade;
    }
    void avegrade(){
        System.out.println("平均成绩为："+(float)((this.jgrade+this.cgrade+this.sgrade)/3.0));
    }
    void allgrade(){
        System.out.println("总成绩为："+(this.jgrade+this.cgrade+this.sgrade));
    }

    public static void main(String[] args) {
        GradeCalculate grade1 = new GradeCalculate();
        grade1.setJgrade(80f);
        grade1.setCgrade(70f);
        grade1.setSgrade(85f);
        grade1.avegrade();
        grade1.allgrade();

        GradeCalculate grade2 = new GradeCalculate(90f,92f,95f);
        grade2.avegrade();
        grade2.allgrade();
    }
}
