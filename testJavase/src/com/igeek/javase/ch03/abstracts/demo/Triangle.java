package com.igeek.javase.ch03.abstracts.demo;

/**
 * @author wangjin
 * 2023/7/14 11:35
 * @description TODO
 */
public class Triangle extends Shape {

    public void work(){
        System.out.println("work~");
    }

    @Override
    public void draw() {
        System.out.println("画"+this.getName());
    }

    public Triangle() {
    }

    public Triangle(String name) {
        super(name);
    }
}

