package com.igeek.javase.ch03.relation.use;

/**
 * @author wangjin
 * 2023/7/13 17:46
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        People people = new People("老王");
        people.play();

        Car car = new Car("特斯拉" , "白色");
        people.work(car);
    }
}
