package com.igeek.demo.ch02.dao;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author chenmin
 * 2023/7/20 10:42
 * @description 具备CRUD的DAO类
 *
 * QueryRunner类常用方法
 * 1.执行增、删、改操作
 * 第一个参数：sql语句   第二个参数：传参的条件
 * public int update(String sql, Object... params) throws SQLException
 * 2.执行查询操作
 * 第一个参数：sql语句   第二个参数：ResultSetHandler结果集处理器    第三个参数：传参的条件
 * public Object query(String sql, ResultSetHandler rsh,Object... params) throws SQLException
 *
 * ResultSetHandler结果集处理器
 * 1.BeanHandler:将结果集的第一行，封装成对象，并返回bean
 *  		   new BeanHandler<T>(Xxx.class)
 * 使用场景：通过ID查询员工信息等
 *
 * 2.BeanListHandler:将结果集中的所有行，封装成对象的集合，并返回List
 *  	       new BeanListHandler<T>(Xxx.class)
 * 使用场景：通过名称模糊查询所有符合的员工信息等
 *
 * 3.ScalarHandler:将结果集中的第一行第一列，以Object返回
 *  		   new ScalarHandler()
 * 使用场景：通过姓名查询女生的手机号、通过ID查询女生的姓名、查询员工表的总记录数等
 *
 * 4.MapListHandler：将多条记录封装到一个Map集合中，以List<Map<String, Object>>返回
 *            new MapListHandler()
 * 使用场景：比较适用于多表查询，例如：查询每个员工的姓名及其所在的部门名称等
 */
public class BaseDao<T> {

    private QueryRunner queryRunner = new QueryRunner();

    //增、删、改
    public int update(Connection conn , String sql , Object... params) throws SQLException {
        int i = queryRunner.update(conn , sql, params);
        return i;
    }

    //查询  查询单个值ScalarHandler
    public Object selectSingleValue(Connection conn , String sql , Object... params) throws SQLException {
        Object query = queryRunner.query(conn , sql, new ScalarHandler(), params);
        return query;
    }

    //查询  查询单个对象BeanHandler
    public T selectOne(Connection conn , String sql , Class<T> clazz ,  Object... params) throws SQLException {
        T t = queryRunner.query(conn , sql , new BeanHandler<>(clazz) , params);
        return t;
    }

    //查询  查询多个对象BeanListHandler
    public List<T> selectAll(Connection conn , String sql , Class<T> clazz ,  Object... params) throws SQLException {
        List<T> list = queryRunner.query(conn , sql, new BeanListHandler<>(clazz), params);
        return list;
    }

    //查询  MapListHandler 将多条记录封装到一个Map集合中，以List<Map<String, Object>>返回
    public List<Map<String,Object>> selectMap(Connection conn , String sql , Object... params) throws SQLException {
        //Map<String, Object> key字段名,value值
        List<Map<String, Object>> mapList = queryRunner.query(conn, sql, new MapListHandler(), params);
        return mapList;
    }
}
