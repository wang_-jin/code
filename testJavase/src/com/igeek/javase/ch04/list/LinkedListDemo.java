package com.igeek.javase.ch04.list;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author wangjin
 * 2023/7/17 14:13
 * @description TODO
 * 作业：
 * 使用LinkedList，模拟完成堆栈操作  先进后出
 */
public class LinkedListDemo {

    public static void main(String[] args) {
        LinkedList<Object> list = new LinkedList<>();
        list.add(12);       //int -> Integer
        list.add(true);
        list.addLast("abc");
        list.add(95.5);
        list.addFirst(null);
        System.out.println(list);

        System.out.println("------迭代------");
        Iterator<Object> iterator = list.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

        System.out.println("--------------");
        //获取最后一个值
        System.out.println(list.getLast());
        //弹出头元素
        System.out.println(list.pop());
        System.out.println(list);
    }

}
