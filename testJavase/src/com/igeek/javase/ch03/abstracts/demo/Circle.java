package com.igeek.javase.ch03.abstracts.demo;

/**
 * @author wangjin
 * 2023/7/14 11:35
 * @description TODO
 */
public class Circle extends Shape{

    public Circle() {
    }

    public Circle(String name) {
        super(name);
    }

    //方法重写
    @Override
    public void draw() {
        System.out.println("画"+this.getName());
    }
}

