package com.igeek.demo.ch01;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * FirstServlet类
 * 1.extends HttpServlet
 * 2.@WebServlet 此注解标注当前类是控制类Servlet
 * 2.1 name属性：当前Servlet的名字，不可以重复
 * 2.2 value属性 、 urlPatterns属性 ：当前Servlet能够处理的请求地址，不可以重复，且必须以/开头
 * 3.重写doGet、doPost
 * Get方式请求
 *    - 超链接<a href></a>、地址栏直接输入、form表单method="get"
 * Post方式请求
 *    - form表单method="post"
 */
@WebServlet(name = "FirstServlet", value = "/first" )
public class FirstServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("doGet");
        this.doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("doPost");
        //1.处理编码集
        // 请求request中文友好
        request.setCharacterEncoding("UTF-8");
        // 响应response中文友好  有可能会导致css样式丢失
        response.setContentType("text/html;charset=UTF-8");
        // 响应response中文友好  针对于响应json数据
        // response.setCharacterEncoding("UTF-8");

        //2.接收请求参数
        //表单中name="username"  username作为key接收数据
        String username = request.getParameter("username");
        System.out.println("username = "+username);
        String password = request.getParameter("password");
        System.out.println("password = "+password);

        //3.HTML响应  --> json数据响应
        /*PrintWriter out = response.getWriter();
        if(username!=null && !username.equals("") && password!=null && !password.equals("")){
            out.println("<h1 style='color:green'>登录成功，欢迎："+username+"</h1>");
        }else{
            out.println("<h1 style='color:red'>登录失败</h1>");
        }
        out.flush();
        out.close();*/

        //4.页面跳转
        /**
         * request.getRequestDispatcher("success.jsp").forward(request,response); 请求转发
         * response.sendRedirect("success.jsp");  响应重定向
         *
         * 请求转发 和 响应重定向 的区别
         * 1.请求转发 延续之前的请求，不会产生新的请求
         * 响应重定向 产生新的请求
         * 2.请求转发 地址栏不变，仍然显示请求地址
         * 响应重定向 地址栏显示的是目标地址
         * 3.请求转发 会携带之前请求中的数据
         * 响应重定向 不会携带之前请求中的数据
         * 4.请求转发 跳转至本项目内的页面
         * 响应重定向 可以跳转至项目外对的页面
         *
         * 使用场景总结：
         * 1.增、删、改 ，建议使用响应重定向，可以避免重复提交表单
         * 2.查询、必须要携带请求数据  ，使用请求转发
         */
        if(username!=null && !username.equals("") && password!=null && !password.equals("")){
            //跳转至成功界面
            request.getRequestDispatcher("success.jsp").forward(request,response);
            //response.sendRedirect("success.jsp");
        }else{
            //跳转至失败界面
            response.sendRedirect("error.jsp");
        }
    }
}
