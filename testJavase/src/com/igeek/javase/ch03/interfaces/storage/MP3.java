package com.igeek.javase.ch03.interfaces.storage;

/**
 * @author wangjin
 * 2023/7/14 15:37
 * @description TODO
 */
public class MP3 implements IStorage{
    @Override
    public void read() {
        System.out.println("MP3开始读数据");
    }

    @Override
    public void write() {
        System.out.println("MP3开始写数据");
    }
    public void MP3Work(){
        System.out.println("MP3正在播放");
    }
}
