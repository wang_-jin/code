package com.igeek.javase.ch04.list;

import java.util.LinkedList;

/**
 * @author wangjin
 * 2023/7/17 23:49
 * @description TODO
 */
public class LinkedListTest {
    public static void main(String[] args) {
        LinkedList<Object> list = new LinkedList<>();
        list.push("a");
        list.push("b");
        list.push("c");
        list.push("d");
        System.out.println(list);
        System.out.println("list.size = "+list.size());
        int length = list.size();
        System.out.println("---------------");

        for (int i = 0; i < length; i++) {
            Object topElement = list.pop();
            System.out.println("topElement = "+topElement);
        }


    }
}
