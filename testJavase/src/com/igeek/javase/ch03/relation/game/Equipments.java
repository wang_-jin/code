package com.igeek.javase.ch03.relation.game;

/**
 * @author wangjin
 * 2023/7/13 17:47
 * @description TODO
 */
public class Equipments {
    private String name;
    private int level;
    private String color;
    private String kind;
    private int power;
    private int defence;


    public Equipments() {
    }

    public Equipments(String name, int level, String color, String kind, int power, int defence) {
        this.name = name;
        this.level = level;
        this.color = color;
        this.kind = kind;
        this.power = power;
        this.defence = defence;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return level
     */
    public int getLevel() {
        return level;
    }

    /**
     * 设置
     * @param level
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * 获取
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * 设置
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * 获取
     * @return kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * 设置
     * @param kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     * 获取
     * @return power
     */
    public int getPower() {
        return power;
    }

    /**
     * 设置
     * @param power
     */
    public void setPower(int power) {
        this.power = power;
    }

    /**
     * 获取
     * @return defence
     */
    public int getDefence() {
        return defence;
    }

    /**
     * 设置
     * @param defence
     */
    public void setDefence(int defence) {
        this.defence = defence;
    }

    public String toString() {
        return "Equipments{name = " + name + ", level = " + level + ", color = " + color + ", kind = " + kind + ", power = " + power + ", defence = " + defence + "}";
    }

    //装备拥有获取装备信息的方法
    public void show(){
        System.out.println("装备的名称："+name);
        System.out.println("装备的等级："+level);
        System.out.println("装备的色系："+color);
        System.out.println("装备的类别："+kind);
        System.out.println("装备的攻击力："+power);
        System.out.println("装备的防御力："+defence);
    }
}
