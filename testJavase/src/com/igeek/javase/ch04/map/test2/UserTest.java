package com.igeek.javase.ch04.map.test2;

import java.util.HashMap;

/**
 * @author wangjin
 * 2023/7/18 0:46
 * @description TODO
 * 作业：
 * 1.封装 add(User元素)  添加User时，姓名和密码一致则判定是同一元素，将不予以添加
 * 2.HashMap<User,String>  put
 */
public class UserTest {

    public static void main(String[] args) {
        User user1 = new User("zss" , "123");
        User user2 = new User("lss" , "123");
        User user3 = new User("zl" , "123");
        User user4 = new User("zh" , "123");
        User user5 = new User("zss" , "123");

        HashMap<User , String> map = new HashMap<>();
        System.out.println(add(map, user1));
        System.out.println(add(map, user2));
        System.out.println(add(map, user3));
        System.out.println(add(map, user4));
        System.out.println(add(map, user5));

        System.out.println(map);
    }

    public static boolean add(HashMap<User,String> map , User user){
        //containsKey 判断是否包含键Key，Key键重写hashCode()和equals()
        if(!map.containsKey(user)){
            map.put(user , user.getUsername());
            return true;
        }
        return false;
    }
}