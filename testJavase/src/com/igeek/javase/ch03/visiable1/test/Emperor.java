package com.igeek.javase.ch03.visiable1.test;

/**
 * @author wangjin
 * 2023/7/13 12:36
 * @description TODO
 */
public class Emperor {
    private String name;
    private String date;
    private String feat;
    private String description;

    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    public String getDate() {return date;}
    public void setDate(String date) {this.date = date;}

    public String getFeat() {return feat;}
    public void setFeat(String feat) {this.feat = feat;}

    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}

    public Emperor() {
    }

    //有参构造
    public Emperor(String name,String date,String feat,String description){
        this.name = name;
        this.date = date;
        this.feat = feat;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Emperor{" +
                "name='" + name + '\'' +
                ", date='" + date + '\'' +
                ", feat='" + feat + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
