package com.igeek.javase.ch02;

/**
 * @author wangjin
 * 2023/7/11 15:12
 * @description TODO
 */
public class ForDemo {
    public static void main(String[] args) {
        //for(初始值;循环条件;步长){}
        /*
         * 1、需求：输出1-100之间的奇数和。（for）
         */
        /*
         * 2、需求：输出1-100之间的偶数和。（for）
         */
        int sum1 = 0;
        int sum2 = 0;
        for (int i=1;i<=100;i++){
            //判断奇数
            if(i%2!=0){
                sum1+=i;  //sum1 = sum1+i
            }

            //判断偶数
            if(i%2==0){
                sum2+=i;
            }
        }
        System.out.println("奇数和："+sum1);
        System.out.println("偶数和："+sum2);
    }

}
