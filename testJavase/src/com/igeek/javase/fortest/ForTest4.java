package com.igeek.javase.fortest;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/7/12 0:04
 * @description 4.编写程序，将一个整数倒排过来，例如：给定整数：49082000，得到的结果：28094。
 */
public class ForTest4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个数：");
        int num = sc.nextInt();
        while(num%10==0){
            num/=10;
        }
        System.out.println("num ="+num);
        while(num!=0){
            int a = num%10;
            System.out.print(+a);
            num/=10;
        }
    }




}

