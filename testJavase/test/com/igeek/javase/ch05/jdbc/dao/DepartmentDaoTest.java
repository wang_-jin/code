package com.igeek.javase.ch05.jdbc.dao;

import com.igeek.javase.ch05.jdbc.entity.Department;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

public class DepartmentDaoTest {

    private DepartmentDao departmentDao;

    @Before
    public void setUp() throws Exception {
        departmentDao = new DepartmentDao();
    }

    @Test
    public void insertDept() throws SQLException {
        Department department = new Department(1 , "aaa" , 100 , 1000);
        int i = departmentDao.insertDept(department);
        System.out.println(i>0?"插入成功":"插入失败");
    }

    @Test
    public void findDeptByName() throws SQLException {
        List<Department> list = departmentDao.findDeptByName("a");
        System.out.println(list);
        System.out.println(list.size());
    }
}