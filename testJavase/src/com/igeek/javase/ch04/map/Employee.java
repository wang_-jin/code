package com.igeek.javase.ch04.map;

/**
 * @author wangjin
 * 2023/7/17 18:38
 * @description TODO
 */
public class Employee implements Comparable<Employee> {

    private String name;
    private double salary;

    public Employee() {
    }

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return salary
     */
    public double getSalary() {
        return salary;
    }

    /**
     * 设置
     * @param salary
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String toString() {
        return "Employee{name = " + name + ", salary = " + salary + "}";
    }

    //比较 按照薪资的升序进行排列，薪资相同则按照名字的升序排列
    @Override
    public int compareTo(Employee o) {
        if(this.salary==o.salary){
            return this.name.compareTo(o.name);
        }
        return Double.compare(this.salary , o.salary);
    }
}
