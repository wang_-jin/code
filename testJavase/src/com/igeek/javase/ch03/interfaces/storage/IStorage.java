package com.igeek.javase.ch03.interfaces.storage;

public interface IStorage {
    void read();
    void write();
}
