package com.igeek.javase.ch03.abstracts.demo;

/**
 * @author wangjin
 * 2023/7/14 11:35
 * @description TODO
 */
public class Test{

    public static void main(String[] args) {
        //Shape shape = new Shape();

        //多态：父类的引用指向子类的实例
        Shape circle = new Circle("椭圆形");
        //编译看左边，运行看右边
        circle.draw();

        Shape triangle = new Triangle("三角形");
        triangle.draw();
        //编译看左边，运行看右边
        if(triangle instanceof Triangle){
            ((Triangle)triangle).work();  //ClassCastException 类型转换异常
        }else{
            System.out.println("当前类型不是三角形");
        }

    }

}

