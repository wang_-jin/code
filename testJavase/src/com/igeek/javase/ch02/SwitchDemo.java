package com.igeek.javase.ch02;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/7/11 12:59
 * @description switch...case案例
 */
public class SwitchDemo {
    /*
     * 需求：switch示例：从键盘接收两个数字,再接收一个符号,然后进行运算.
     * 如果输入的是错误的字符,不进行计算而是提示不存在字符
     * -------请输入第一个数字:8
     * -------请输入第二个数字:9
     * -------请输入运算符号:+
     * -------8+9=17
     */
    public static void main(String[] args) {
        //创建Scanner对象，获取两个数字
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入第一个数字");
        int i = sc.nextInt();
        System.out.println("请输入第二个数字");
        int j = sc.nextInt();
        //获取一个运算符号
        System.out.println("请输入一个运算符号");
        String s = sc.next();
        //System.out.println("s = "+s);


        //进行运算
        switch (s){
            case "+":
                System.out.println("i + j = "+ (i+j));
                break;//跳出当前分支
            case "-":
                System.out.println("i - j = "+ (i-j));
                break;//跳出当前分支
            case "*":
                System.out.println("i * j = "+ (i*j));
                break;//跳出当前分支
            case "/":
                System.out.println("i / j = "+ (i/j));
                break;//跳出当前分支
            default:
                System.out.println("运算符号不存在~");
        }
    }
}
