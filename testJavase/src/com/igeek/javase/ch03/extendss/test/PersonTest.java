package com.igeek.javase.ch03.extendss.test;

/**
 * @author wangjin
 * 2023/7/13 13:30
 * @description TODO
 */
public class PersonTest {
    public static void main(String[] args) {
        Person p1 = new Person("张三",26,false,null);
        Person p2 = new Person("李四",25,false,null);
        Person p3 = new Person("王五",30,true,null);
        p1.marry(p3);
        p2.marry(p3);

    }
}
