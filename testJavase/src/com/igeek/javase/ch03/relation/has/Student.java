package com.igeek.javase.ch03.relation.has;

/**
 * @author wangjin
 * 2023/7/13 12:34
 * @description TODO
 */
public class Student {
    private String stuName;
    private int level;

    //一个学生会有一个所属的班级  关联关系：一对一  双向关联  弱关联
    private Classes classes;

    public Student() {
    }

    public Student(String stuName, int level) {
        this.stuName = stuName;
        this.level = level;
    }

    public Student(String stuName, int level, Classes classes) {
        this.stuName = stuName;
        this.level = level;
        this.classes = classes;
    }

    /**
     * 获取
     * @return stuName
     */
    public String getStuName() {
        return stuName;
    }

    /**
     * 设置
     * @param stuName
     */
    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    /**
     * 获取
     * @return level
     */
    public int getLevel() {
        return level;
    }

    /**
     * 设置
     * @param level
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * 获取
     * @return classes
     */
    public Classes getClasses() {
        return classes;
    }

    /**
     * 设置
     * @param classes
     */
    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    public void show(){
        System.out.println("学生姓名："+this.stuName);
        System.out.println("学生年级："+this.level);
        System.out.println("学生所在班级："+this.classes.getClassName());
    }

    /*public String toString() {
        return "Student{stuName = " + stuName + ", level = " + level + ", classes = " + classes + "}";
    }*/
}
