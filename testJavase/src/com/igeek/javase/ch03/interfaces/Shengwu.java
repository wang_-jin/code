package com.igeek.javase.ch03.interfaces;
/**
 * 接口
 * 1.声明接口 关键字interface
 * 2.接口中不允许存在普通的属性，只允许存在公开的、静态的常量
 * 3.接口中不允许存在普通的方法，JDK1.7及以前只有抽象方法
 * （JDK1.8 default默认方法、static静态方法 ； JDK1.9新增私有方法private）
 * 4.接口中不允许有构造方法
 * 5.接口只能作为父类的类型存在，不可以进行实例化，只能等待被实现
 * 6.接口与接口之间允许多继承
 *
 * 面向接口编程
 */

public interface Shengwu {

    //ctrl + shift + U 大写
    //公开的public、静态的static、常量final  可写可不写
    public static final int LEVEL = 2;

    //公开的public、抽象方法abstract 可写可不写
    public abstract void breath();

}