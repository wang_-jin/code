package com.igeek.javase.ch03.extendss;

/**
 * @author wangjin
 * 2023/7/13 10:32
 * @description TODO
 */
public class FatherTest {

    public static void main(String[] args) {
        //多态  父类的引用指向子类的实例
        GrandFather g = new Father("粑粑" , 50 , "司机");
        //编译看左边，运行看右边
        g.show(null);
    }

}