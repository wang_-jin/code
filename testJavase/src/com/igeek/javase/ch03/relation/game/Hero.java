package com.igeek.javase.ch03.relation.game;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/7/13 17:47
 * @description TODO
 */
public class Hero extends Role{


    //一个英雄可以有三个装备  关联关系：一对多
    private Equipments[] equipments = new Equipments[3];
    //统计装备数量
    private int index;

    //攻击方法
    public void attack(Monster monster){
        //有一定几率打出暴击率
        int n = (int)(Math.random()*10+1);
        int block = 0;
        if(n>=3){
            block = n*3;
        }

        //怪兽的失血量 = 英雄攻击力 + 暴击率 - 怪兽的防御力
        int blood = this.getPower() + block - monster.getDefence();
        if(blood>0){
            monster.setBlood(monster.getBlood() - blood);
            System.out.println("当前怪兽失血量为："+blood+"，剩余血量："+monster.getBlood());
        }else{
            System.out.println("此回合攻击无效");
        }
    }

    //死亡判定方法
    public boolean isDead(){
        if(this.getBlood()<=0){
            System.out.println("当前英雄已死亡");
            return true;
        }
        return false;
    }

    //添加装备的方法
    public void addEquip(Equipments e){
        if(index>=3){
            System.out.println("装备已满~");
            return;
        }
        equipments[index++] = e;
        //添加装备，合并装备的攻击力
        this.setPower(this.getPower()+e.getPower());
        //添加装备，合并装备的防御力
        this.setDefence(this.getDefence()+e.getDefence());
    }

    //显示信息的方法
    public void show(){
        System.out.println("英雄的昵称："+getNickName());
        System.out.println("英雄的等级："+getLevel());
        System.out.println("英雄的血量："+getBlood());
        System.out.println("英雄的攻击力："+getPower());
        System.out.println("英雄的防御力："+getDefence());
        System.out.println("英雄的类别："+getKind());

        System.out.println("---------英雄装备的信息----------");
        if(equipments!=null){
            for (int i = 0; i < equipments.length; i++) {
                if(equipments[i]!=null){
                    equipments[i].show();
                }
            }
        }
    }

    public Hero() {
    }
    public Hero(String nickName, int level, int blood, int power, int defence, String kind) {
        super(nickName,level,blood,power,defence,kind);

    }

    public Hero(String nickName, int level, int blood, int power, int defence, String kind, Equipments[] equipments) {
        super(nickName,level,blood,power,defence,kind);
        this.equipments = equipments;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * 获取
     * @return equipments
     */
    public Equipments[] getEquipments() {
        return equipments;
    }

    /**
     * 设置
     * @param equipments
     */
    public void setEquipments(Equipments[] equipments) {
        this.equipments = equipments;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "equipments=" + Arrays.toString(equipments) +
                ", index=" + index +
                "} " + super.toString();
    }
}
