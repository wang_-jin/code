package com.igeek.javase.ch05.jdbc.dao;

import com.igeek.javase.ch05.jdbc.entity.Department;
import com.igeek.javase.ch05.jdbc.entity.Girl;
import com.igeek.javase.ch05.jdbc.utils.JDBCUtilsByC3P0;

import java.sql.SQLException;
import java.util.List;

/**
 * @author wangjin
 * 2023/7/20 11:36
 * @description TODO
 */
public class GirlsDao extends BaseDao<Girl>{
    //3.添加女生信息
    public int insertGirl(Girl girl) throws SQLException {
        String sql = "insert into girl values(null,?,default,?,?,null,?)";
        int i = this.update(JDBCUtilsByC3P0.getConn() , sql,  girl.getName(),
                girl.getBorndate(), girl.getPhone(),  girl.getBoyfriendId());
        return i;
    }
    //5.通过ID删除女生信息
    public int deleteGirl(int id) throws SQLException {
        String sql = "delete from girl where id = ?";
        int i = this.update(JDBCUtilsByC3P0.getConn(), sql, id);
        return i;
    }
    //6.根据手机号查询女孩信息
    public List<Girl> findGirlByPhone(String phone) throws SQLException {
        String sql = "select * from girl where phone = ?";
        List<Girl> girl = this.selectAll(JDBCUtilsByC3P0.getConn(), sql, Girl.class, phone);
        return girl;
    }
    //7.查询名字中含有 小 字的女生个数
    public int selectCount(String query) throws SQLException {
        String sql = "select count(*) from girl where name like concat('%', ? ,'%')";
        Long count = (Long)this.selectSingleValue(JDBCUtilsByC3P0.getConn(), sql, query);
        return count.intValue();
    }
}
