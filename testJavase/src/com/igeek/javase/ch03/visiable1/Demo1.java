package com.igeek.javase.ch03.visiable1;

/**
 * @author wangjin
 * 2023/7/12 15:59
 * @description TODO
 * 访问权限修饰符
 *   1.public 公开的访问权限，可以修饰类、属性、方法
 *   2.protected 受保护的访问权限，可以修饰属性、方法
 *   3.(default) 默认的访问权限，可以修饰类、属性、方法
 *   4.private   私有的访问权限，可以修饰属性、方法
 *
 *              本类   同包下其它类   不同包下的子类   不同包下的其它类
 * public       OK       OK           OK             OK
 * protected    OK       OK           OK
 * (default)    OK       OK
 * private      OK
 * 访问权限由大到小： public > protected > (default) > private
 */
//本类
public class Demo1 {
    //属性
    protected String name;

    //方法
    protected void eat(){
        System.out.println(name+"吃饭~");
    }

    public static void main(String[] args) {
        Demo1 d = new Demo1();
        d.name = "张三";
        d.eat();
    }
}
