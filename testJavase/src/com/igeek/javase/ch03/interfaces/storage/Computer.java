package com.igeek.javase.ch03.interfaces.storage;

/**
 * @author wangjin
 * 2023/7/14 15:36
 * @description TODO
 */
public class Computer {
    private  IStorage iStorage;

     public Computer(IStorage iStorage){
         this.iStorage = iStorage;
     }
    public void readData() {
        System.out.println("电脑开始读数据");
        iStorage.read();
    }

    public void writeData() {
        System.out.println("电脑开始写数据");
        iStorage.write();
    }
}
