package com.igeek.javase.ch04.map;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author wangjin
 * 2023/7/17 18:37
 * @description TODO
 * put流程  hash -> equals -> 是否添加元素
 *
 * 作业：
 * 1.封装 add(User元素)  添加User时，姓名和密码一致则判定是同一元素，将不予以添加
 * 2.HashMap<User,String>  put
 */
public class StudentHashMap {

    public static void main(String[] args) {
        //以Student作为Key，以其名字作为Value
        Map<Student , String> map  = new HashMap<>();

        Student stu1 = new Student("abd", 21, 2);
        Student stu2 = new Student("abb", 18, 1);
        Student stu3 = new Student("aba", 20, 3);
        Student stu4 = new Student("abb", 18, 1);
        System.out.println(stu2);  //com.igeek.javase.ch04.map.Student@1b6d3586  --> 5855aaf
        System.out.println(stu4);  //com.igeek.javase.ch04.map.Student@4554617c  --> 5855aaf
        System.out.println(stu2 == stu4);       //==比较的是地址 false
        System.out.println(stu2.equals(stu4));  //默认用的Object类中equals()，则比较的是地址 false  --> true

        //添加元素，  要求当学生的姓名、年龄、年级一致时，认定是同一元素，不予以重复添加
        map.put(stu1, "abd");
        map.put(stu2, "abb");
        map.put(stu3, "aba");
        map.put(stu4, "abb");
        //迭代元素
        Set<Student> keys = map.keySet();
        for (Student key : keys) {
            System.out.println("key = "+key+" , value = "+map.get(key));
        }
    }

}
