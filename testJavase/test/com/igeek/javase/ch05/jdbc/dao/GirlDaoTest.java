package com.igeek.javase.ch05.jdbc.dao;

import com.igeek.javase.ch05.jdbc.entity.Girl;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class GirlDaoTest {

    private GirlDao girlDao;

    @Before
    public void setUp() throws Exception {
        girlDao = new GirlDao();
    }

    @Test
    public void selectOneById() throws SQLException, ClassNotFoundException {
        Girl girl = girlDao.selectOneById(1);
        System.out.println(girl);
    }

    @Test
    public void selectAll() throws SQLException, ClassNotFoundException {
        List<Girl> girls = girlDao.selectAllByName("小");
        System.out.println(girls);
    }

    @Test
    public void updateGirl() throws SQLException {
        int i = girlDao.updateGirl("木婉清", "11122233355");
        System.out.println(i>0?"更新成功":"更新失败");
    }

    @Test
    public void selectOnePhone() throws SQLException, ClassNotFoundException {
        Girl girl = girlDao.selectOneByPhone("18209876577");
        System.out.println(girl);
    }

    @Test
    public void deleteOneById() throws SQLException {
        int i = girlDao.deleteOneById(12);
        System.out.println(i>0?"删除成功":"删除失败");
    }

    @Test
    public void insertGirl() throws SQLException, ParseException {
        String str = "2001-02-03";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date borndate = sdf.parse(str);
        java.sql.Date d = new java.sql.Date(borndate.getTime());
        int i = girlDao.insertGirl(12 , "小绿" , "女" , d, "18355564556" , 6);
        System.out.println(i>0?"添加成功":"添加失败");
    }
}