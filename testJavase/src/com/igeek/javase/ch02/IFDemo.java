package com.igeek.javase.ch02;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/7/11 11:56
 * @description if案例
 */
public class IFDemo {
    /*
     * 需求：if示例,要求从键盘接受两个数字,进行比较,输出较大的数字
     * 1.Scanner对象  Scanner sc=new Scanner(System.in);
     * 2.从键盘接受两个数字  int nextInt()  整型int
     * 3.if判断，输出较大的数字
     */
    public static void main(String[] args) {
        //通过Scanner扫描器，获取控制台输入的数字
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入第一个数字：");
        int a = scanner.nextInt();
        System.out.println("请输入第二个数字：");
        int b = scanner.nextInt();
        System.out.println("a = "+a);
        System.out.println("b = "+b);

        //比较两个数字的大小
        if (a>b){
            System.out.println("Max = "+a);
        }else{
            System.out.println("Max = "+b);
        }

    }

}
