package com.igeek.javase.ch04.map.test1;

import java.util.TreeMap;

/**
 * @author wangjin
 * 2023/7/18 0:52
 * @description TODO
 * User  name , age
 * 按照姓名的升序进行排列，姓名相同则按照年龄的降序进行排序，都姓名和年龄都相同，则不进行存储
 */
public class UserInfoTest {

    public static void main(String[] args) {
        TreeMap<UserInfo , Integer> map = new TreeMap<>();

        UserInfo user1 = new UserInfo("ddd" , 20);
        UserInfo user2 = new UserInfo("bab" , 18);
        UserInfo user3 = new UserInfo("bcb" , 21);
        UserInfo user4 = new UserInfo("bbb" , 22);
        UserInfo user5 = new UserInfo("bbb" , 22);
        UserInfo user6 = new UserInfo("ddd" , 30);

        map.put(user1 , user1.getAge());
        map.put(user2 , user2.getAge());
        map.put(user3 , user3.getAge());
        map.put(user4 , user4.getAge());
        map.put(user5 , user5.getAge());
        map.put(user6, user6.getAge());

        System.out.println(map);
    }

}
