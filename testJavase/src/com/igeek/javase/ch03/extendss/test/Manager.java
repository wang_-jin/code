package com.igeek.javase.ch03.extendss.test;

/**
 * @author wangjin
 * 2023/7/13 13:30
 * @description TODO
 */
public class Manager extends Employee{
    private String departments;


    public Manager() {
    }

    public Manager(String name, String address, double salary, String departments) {
        super(name, address, salary);
        this.departments = departments;
    }

    /**
     * 获取
     * @return departments
     */
    public String getDepartments() {
        return departments;
    }

    /**
     * 设置
     * @param departments
     */
    public void setDepartments(String departments) {
        this.departments = departments;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "departments='" + departments + '\'' +
                "} " + super.toString();
    }
}
