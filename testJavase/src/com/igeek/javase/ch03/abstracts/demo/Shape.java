package com.igeek.javase.ch03.abstracts.demo;

/**
 * @author wangjin
 * 2023/7/14 11:35
 * @description TODO
 * 抽象：没有特定的描述信息
 *
 * 抽象类
 * 1.abstract class
 * 2.在抽象类中，可以有属性、构造方法、方法等
 * 3.一个抽象类中可以没有抽象方法；但是一旦类中有抽象方法则当前类必须是抽象类
 * 4.抽象类不允许被实例化的，只能当做基类（父类）
 * 5.final最终的、abstract抽象的 不可以同时使用
 * 6.一旦子类extends继承抽象类，要么实现抽象方法，要么自己也变成抽象类
 */
public /*final*/ abstract class Shape {

    private String name;

    public void player(){
        System.out.println("player~");
    }

    /*
        抽象方法
        1.方法上 abstract 修饰符
        2.方法没有方法体
        3.final最终的 、 abstract抽象的 不能一起修饰方法
        4.static静态的（只属于类）、abstract抽象的  不能一起修饰方法
     */
    public /*final*/ /*static*/ abstract void draw();

    public Shape(){

    }

    public Shape(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Shape{" +
                "name='" + name + '\'' +
                '}';
    }
}
