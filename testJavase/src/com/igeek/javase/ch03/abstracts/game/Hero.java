package com.igeek.javase.ch03.abstracts.game;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/7/13 17:47
 * @description TODO
 * 定义一个英雄类Hero，可以通过这个类创建英雄角色
 * 属性：
 *     游戏角色拥有昵称name，等级level，血量blood，攻击力power，防御力defence，种族kind，
 * 装备Equip（只能装3个装备，初始没有装备）。
 * 方法：
 *     游戏角色拥有基本攻击方法（拥有一定几率打出暴击）attack(Monster m)，死亡判定方法isDead()，添加装备的方法addEquip(Equip e)，获取角色信息的方法show()。
 */
public class Hero extends Role {


    //一个英雄可以有三个装备  关联关系：一对多
    private Equipments[] equipments = new Equipments[3];
    //统计装备数量
    private int index;

    //攻击方法
    @Override
    public void attack(Role role){
        //有一定几率打出暴击率
        int n = (int)(Math.random()*10+1);
        int block = 0;
        if(n>=equipments.length){
            block = n*3;
        }

        //怪兽的失血量 = 英雄攻击力 + 暴击率 - 怪兽的防御力
        int blood = this.getPower() + block - role.getDefence();
        if(blood>0){
            role.setBlood(role.getBlood() - blood);
            System.out.println("当前怪兽失血量为："+blood+"，剩余血量："+role.getBlood());
        }else{
            System.out.println("此回合攻击无效");
        }
    }



    //添加装备的方法
    public void addEquip(Equipments e){
        if(index>=equipments.length){
            System.out.println("装备已满~");
            return;
        }
        equipments[index++] = e;
        //添加装备，合并装备的攻击力
        this.setPower(this.getPower()+e.getPower());
        //添加装备，合并装备的防御力
        this.setDefence(this.getDefence()+e.getDefence());
    }

    //显示信息的方法
    @Override
    public void show(){
        super.show();

        System.out.println("---------英雄装备的信息----------");
        if(equipments!=null){
            for (int i = 0; i < equipments.length; i++) {
                if(equipments[i]!=null){
                    equipments[i].show();
                }
            }
        }
    }

    public Hero() {
    }
    public Hero(String nickName, int level, int blood, int power, int defence, String kind) {
        super(nickName,level,blood,power,defence,kind);

    }

    public Hero(String nickName, int level, int blood, int power, int defence, String kind, Equipments[] equipments) {
        super(nickName,level,blood,power,defence,kind);
        this.equipments = equipments;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * 获取
     * @return equipments
     */
    public Equipments[] getEquipments() {
        return equipments;
    }

    /**
     * 设置
     * @param equipments
     */
    public void setEquipments(Equipments[] equipments) {
        this.equipments = equipments;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "equipments=" + Arrays.toString(equipments) +
                ", index=" + index +
                "} " + super.toString();
    }
}
