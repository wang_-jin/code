package com.igeek.javase.ch03.relation.use;

/**
 * @author wangjin
 * 2023/7/13 17:46
 * @description TODO
 * 依赖关系
 * 1.语法：一个类作为另外一个类中方法的形参类型存在
 * 具备临时性、偶然性
 * 2.use-a
 */
public class People {
    private String name;

    //依赖关系
    public void work(Car car){
        System.out.println(name+"上班去~~");
        car.run();
    }

    public void play(){
        System.out.println(name+"出去玩~");
    }

    public People() {
    }

    public People(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "People{name = " + name + "}";
    }
}
