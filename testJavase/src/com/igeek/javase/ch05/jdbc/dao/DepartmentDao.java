package com.igeek.javase.ch05.jdbc.dao;

import com.igeek.javase.ch05.jdbc.entity.Department;
import com.igeek.javase.ch05.jdbc.utils.JDBCUtilsByC3P0;

import java.sql.SQLException;
import java.util.List;

/**
 * @author chenmin
 * 2023/7/20 11:01
 * @description TODO
 */
public class DepartmentDao extends BaseDao<Department>{

    //插入部门信息
    public int insertDept(Department department) throws SQLException {
        String sql = "insert into departments values(?,?,?,?)";
        int i = this.update(JDBCUtilsByC3P0.getConn() , sql, department.getDepartment_id(), department.getDepartment_name(),
                department.getManager_id(), department.getLocation_id());
        return i;
    }

    //通过部门名称模糊查询信息
    public List<Department> findDeptByName(String name) throws SQLException {
        String sql = "select * from departments where department_name like concat('%',?,'%')";
        List<Department> departments = this.selectAll(JDBCUtilsByC3P0.getConn(), sql, Department.class, name);
        return departments;
    }
}
