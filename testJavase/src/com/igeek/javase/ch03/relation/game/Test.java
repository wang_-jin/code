package com.igeek.javase.ch03.relation.game;

import java.util.concurrent.TimeUnit;

/**
 * @author wangjin
 * 2023/7/13 17:47
 * @description TODO
 */
public class Test {
    public static void main(String[] args) throws InterruptedException {
        Hero hero = new Hero("剑圣" , 7 , 1000 , 280 , 60 , "兽族");
        Monster monster = new Monster("哥斯拉" , 7 , 2000 , 200 , 100 , "怪兽族");
        Equipments equip1 = new Equipments("倚天剑" , 9 , "金色" , "剑类" , 80 , 30);
        Equipments equip2 = new Equipments("屠龙刀" , 9 , "银色" , "刀类" , 70 , 40);
        Equipments equip3 = new Equipments("盾牌" , 9 , "彩色" , "盾类" , 10 , 90);

        //添加装备
        hero.addEquip(equip1);
        hero.addEquip(equip3);

        //hero.show();
        //monster.show();

        //回合制攻击
        int count = 0;
        while (true){
            System.out.println("================第"+(++count)+"回合================");

            //英雄攻击
            hero.attack(monster);
            if(monster.isDead()){
                System.out.println("游戏结束，怪兽噶了~");
                System.exit(0);
            }

            TimeUnit.SECONDS.sleep(1);

            //怪兽攻击
            monster.attack(hero);
            if(hero.isDead()){
                System.out.println("游戏结束，英雄噶了~");
                System.exit(0);
            }

            TimeUnit.SECONDS.sleep(1);
        }

    }
}
