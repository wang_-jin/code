package com.igeek.javase.ch03.obj;

/**
 * @author wangjin
 * 2023/7/12 15:18
 * @description TODO
 */
public class Car {
    String brand;
    int price;
    String color;

    public Car(String brand, int price, String color){
        this.brand = brand;
        this.price = price;
        this.color = color;
    }
    //行为
    void run(){
        System.out.println(price+"的"+brand+"车正在行驶");
    }
    void stop(){
        System.out.println(color+brand+"车停在了路上");
    }

    public static void main(String[] args) {
        Car ca1 = new Car("宝马",2000000,"红色");
        ca1.run();
        Car ca2 = new Car("比亚迪",3000000,"白色");
        ca2.stop();
    }

}
