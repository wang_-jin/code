package com.igeek.javase.ch03.abstracts.game;

/**
 * @author wangjin
 * 2023/7/13 17:47
 * @description TODO
 * 定义一个怪兽类Monster，可以通过这个类创建怪兽角色
 * 属性：
 *     怪兽拥有名称name，等级level，血量blood，攻击力power，防御力defence，种族kind。
 * 方法：
 *     怪兽拥有基本攻击方法attack(Hero h)，死亡判定方法isDead()，获取怪兽信息的方法show()。
 */
public class Monster extends Role {


    public Monster() {
    }

    public Monster(String nickName, int level, int blood, int power, int defence, String kind) {
        super(nickName, level, blood, power, defence, kind);
    }

    //基本攻击方法
    @Override
    public void attack(Role role){
        //英雄的失血量 = 怪兽的攻击力 - 英雄的防御力
        int blood = this.getPower() - role.getDefence();
        if(blood>=0){
            role.setBlood(role.getBlood() - blood);
            System.out.println("英雄本回合失血量："+blood+"，剩余血量："+role.getBlood());
        }else{
            System.out.println("此回合攻击无效");
        }
    }
}
