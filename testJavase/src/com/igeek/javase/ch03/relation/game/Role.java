package com.igeek.javase.ch03.relation.game;

/**
 * @author wangjin
 * 2023/7/13 21:28
 * @description TODO
 * 二.游戏
 * 1、定义一个英雄类Hero，可以通过这个类创建游戏角色，
 * 游戏角色拥有昵称，等级，血量，攻击力，防御力，种族，装备（只能装3个装备，初始没有装备）。
 * 游戏角色拥有基本攻击方法（拥有一定几率打出暴击），死亡判定方法，添加装备的方法，
 * 获取角色信息的方法。
 *
 * 2、定义一个怪兽类Monster，可以通过这个类创建各种怪兽，
 * 怪兽拥有名称，等级，血量，攻击力，防御力，种族。
 * 怪兽拥有基本攻击方法，死亡判定方法，获取怪兽信息的方法。
 *
 * 3、定义装备类Equip，可以通过这个类创建各种装备
 * 装备拥有名称，级别，颜色，类别，攻击力，防御力
 * 装备拥有获取装备信息的方法
 *
 * 4、测试类
 * 创建一个游戏角色英雄类Hero、装备Equip、怪兽Monster，
 * 角色英雄类Hero装上装备，双方使用回合制的形式进行攻击
 * （一人打一下，角色英雄类Hero先攻击，失血量=攻击-防御 ，攻击<防御，不产生攻击），
 * 直到一方死亡，游戏结束
 *
 * 暴击>某个值
 * 失血量=(攻击+暴击)-防御
 * 1.Game类：
 * 提供一个角色类Role，英雄类Hero和怪兽类Monster继承Role
 * Role类（属性，方法）
 */
public class Role {
    private String nickName;
    private int level;
    private int blood;
    private int power;
    private int defence;
    private String kind;


    public Role() {
    }

    public Role(String nickName, int level, int blood, int power, int defence, String kind) {
        this.nickName = nickName;
        this.level = level;
        this.blood = blood;
        this.power = power;
        this.defence = defence;
        this.kind = kind;
    }

    /**
     * 获取
     * @return nickName
     */
    public String getNickName() {
        return nickName;
    }

    /**
     * 设置
     * @param nickName
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /**
     * 获取
     * @return level
     */
    public int getLevel() {
        return level;
    }

    /**
     * 设置
     * @param level
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * 获取
     * @return blood
     */
    public int getBlood() {
        return blood;
    }

    /**
     * 设置
     * @param blood
     */
    public void setBlood(int blood) {
        this.blood = blood;
    }

    /**
     * 获取
     * @return power
     */
    public int getPower() {
        return power;
    }

    /**
     * 设置
     * @param power
     */
    public void setPower(int power) {
        this.power = power;
    }

    /**
     * 获取
     * @return defence
     */
    public int getDefence() {
        return defence;
    }

    /**
     * 设置
     * @param defence
     */
    public void setDefence(int defence) {
        this.defence = defence;
    }

    /**
     * 获取
     * @return kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * 设置
     * @param kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    public String toString() {
        return "Role{nickName = " + nickName + ", level = " + level + ", blood = " + blood + ", power = " + power + ", defence = " + defence + ", kind = " + kind + "}";
    }
}
