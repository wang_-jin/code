package com.igeek.javase.ch03.interfaces.fly;

/**
 * @author wangjin
 * 2023/7/14 15:41
 * @description 飞
 * 1、定义一个接口CanFly，描述会飞的方法public void fly();　
 * 2、分别定义类飞机和鸟，实现CanFly接口。　
 * 3、定义一个测试类，测试飞机和鸟。
 * 测试类中定义一个makeFly()方法，让会飞的事物飞起来。　
 * 4、然后在main方法中创建飞机对象和鸟对象，
并在main方法中调用makeFly()方法，让飞机和鸟起飞。
 */

public class Test {

   /*    多态参数
    1.static修饰的方法，无须产生类的实例对象就可以调用该方法，即satic变量是存储在静态存储区的，不需要实例化。
    2.非static修饰的方法，需要产生一个类的实例对象才可以调用该方法。
    也就是说，在main函数中调用函数只能调用静态的。
    如果要调用非静态的，那么必须要先实例化对象，然后通过对象来调用非静态方法，
    */

    public static void makeFly(CanFly canFly){
        canFly.fly();
    }
    public static void main(String[] args) {

        CanFly p1 = new AirPlane();
        CanFly p2 = new Bird();

        makeFly(p1);
        makeFly(p2);

       /* makeFly(bird);
        System.out.println("----------------");
        makeFly(airplane);*/

    }

}

