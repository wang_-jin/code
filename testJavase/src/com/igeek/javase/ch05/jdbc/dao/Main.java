package com.igeek.javase.ch05.jdbc.dao;

import com.igeek.javase.ch03.interfaces.fly.Test;

import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/7/20 19:00
 * @description TODO
 */
public class Main {

    public void operate() throws SQLException {
        Scanner scanner = new Scanner(System.in);
        int command = scanner.nextInt();
        System.out.println("=========请选择操作键=========");
        switch (command){
            case 1:
                System.out.println("统计学生人数");

            case 2:
                System.out.println("查看学生名单");

            case 3:
                System.out.println("按学号查询学生姓名");

            case 4:
                System.out.println("按姓名查询学生信息");

            case 5:
                System.out.println("修改学生出生日期");

            case 6:
                System.out.println("删除学生记录");

            case 0:
                System.out.println("退出");

        }
    }
}
