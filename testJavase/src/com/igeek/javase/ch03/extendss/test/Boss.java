package com.igeek.javase.ch03.extendss.test;

/**
 * @author wangjin
 * 2023/7/13 13:29
 * @description TODO
 */
public class Boss extends Employee {
    private int bonus;


    public Boss() {
    }

    public Boss(String name, String address, double salary, int bonus) {
        super(name, address, salary);
        this.bonus = bonus;
    }

    /**
     * 获取
     * @return bonus
     */
    public int getBonus() {
        return bonus;
    }

    /**
     * 设置
     * @param bonus
     */
    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return "Boss{" +
                "bonus=" + bonus +
                "} " + super.toString();
    }
}












