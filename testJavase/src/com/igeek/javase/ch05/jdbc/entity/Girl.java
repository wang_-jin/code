package com.igeek.javase.ch05.jdbc.entity;

import java.sql.Blob;
import java.util.Date;

/**
 * @author chenmin
 * 2023/7/19 14:52
 * @description 女生信息
 */
public class Girl {

    private int id;
    private String name;
    private String sex;
    private Date borndate;
    private String phone;
    private Blob photo;
    private int boyfriendId;

    public Girl() {
    }

    public Girl( String name,  Date borndate, String phone,  int boyfriendId) {

        this.name = name;
        this.borndate = borndate;
        this.phone = phone;
        this.boyfriendId = boyfriendId;
    }

    public Girl(int id, String name, String sex, Date borndate, String phone, Blob photo, int boyfriendId) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.borndate = borndate;
        this.phone = phone;
        this.photo = photo;
        this.boyfriendId = boyfriendId;
    }

    /**
     * 获取
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * 设置
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * 设置
     * @param sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * 获取
     * @return borndate
     */
    public Date getBorndate() {
        return borndate;
    }

    /**
     * 设置
     * @param borndate
     */
    public void setBorndate(Date borndate) {
        this.borndate = borndate;
    }

    /**
     * 获取
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 获取
     * @return photo
     */
    public Blob getPhoto() {
        return photo;
    }

    /**
     * 设置
     * @param photo
     */
    public void setPhoto(Blob photo) {
        this.photo = photo;
    }

    /**
     * 获取
     * @return boyfriendId
     */
    public int getBoyfriendId() {
        return boyfriendId;
    }

    /**
     * 设置
     * @param boyfriendId
     */
    public void setBoyfriendId(int boyfriendId) {
        this.boyfriendId = boyfriendId;
    }

    public String toString() {
        return "Girl{id = " + id + ", name = " + name + ", sex = " + sex + ", borndate = " + borndate + ", phone = " + phone + ", photo = " + photo + ", boyfriendId = " + boyfriendId + "}";
    }
}
