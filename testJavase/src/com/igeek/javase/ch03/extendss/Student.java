package com.igeek.javase.ch03.extendss;

/**
 * @author wangjin
 * 2023/7/12 17:39
 * @description 学生类
 * 继承的好处
 * 1.代码复用性提高
 * 2.代码的维护性提高
 *
 * 继承 extends
 * 1.Java类与类 继承关系，单继承
 * 2.super 关键字，父类的关键字
 * super.父类属性  super.父类方法  super()调用父类的构造方法
 * 3.this  关键字，本类对象，代表正在创建的对象或者正在使用的对象
 * this.属性  this.方法  this()调用本类的构造方法
 * 4.父类中的成员属性和成员方法，子类都拥有。而且子类还可以拥有自己的个性。
 */
public class Student extends Person {

    //属性
    private int level;

    //构造方法
    public Student() {
    }

    public Student(String name, int age, char gender, int level) {
        //super 父类的关键字  调用父类的构造方法
        super(name, age, gender);
        this.level = level;
    }

    //getter和setter
    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    //方法
    public void study(){
        System.out.println(this.getName()+"正在学习~");
    }
}
