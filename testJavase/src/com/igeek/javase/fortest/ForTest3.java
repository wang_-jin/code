package com.igeek.javase.fortest;

/**
 * @author wangjin
 * 2023/7/11 23:37
 * @description 3.乘法口诀表；（for）
 */
public class ForTest3 {
    public static void main(String[] args) {
        for (int i=1;i<10;i++){
            for (int j=1;j<=i;j++){
                System.out.print(i+"*"+j+"="+(i*j)+"\t");
            }
            System.out.println();
        }
    }
}
