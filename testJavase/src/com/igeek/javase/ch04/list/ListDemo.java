package com.igeek.javase.ch04.list;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangjin
 * 2023/7/17 0:20
 * @description TODO
 * List 接口
 * 1.保留原存储顺序
 * 2.允许重复插入
 * 3.允许存储null
 *
 * 1.ArrayList
 *    - 基于数组的数据结构实现
 *    - 查询快，增删慢
 *    - 线程不安全
 *    - 使用场景：分页条件查询列表等
 *
 * 2.LinkedList
 *    - 基于链表的数据结构实现
 *    - 查询慢，增删快
 *    - 线程不安全
 *    - 使用场景：浏览足迹等
 *
 * 3.Vector
 * 线程安全
 *
 *
 * ctrl + N    搜索API类
 * ctrl + F12  搜索类中的方法
 * ctrl + 鼠标左键点击  进入方法
 * ctrl + alt + 向左箭头  回去上一步
 */
public class ListDemo {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("123");
    }

}
