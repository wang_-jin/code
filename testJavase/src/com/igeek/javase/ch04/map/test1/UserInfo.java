package com.igeek.javase.ch04.map.test1;

/**
 * @author wangjin
 * 2023/7/18 0:51
 * @description TODO
 * 作业：
 * User  name , age
 * 按照姓名的升序进行排列，姓名相同则按照年龄的降序进行排序，都姓名和年龄都相同，则不进行存储
 */
public class UserInfo implements Comparable<UserInfo> {

    private String name;
    private int age;

    public UserInfo() {
    }

    public UserInfo(String name, int age) {
        this.name = name;
        this.age = age;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "UserInfo{name = " + name + ", age = " + age + "}";
    }

    @Override
    public int compareTo(UserInfo o) {
        if(this.name.equals(o.name)){
            //return o.age-this.age;
            return Integer.compare(o.age , this.age);
        }
        return this.name.compareTo(o.name);
    }
}